+++
title = "More details regarding the Smart Devices SmartV5 and SmartV7 MIDs"
aliases = ["2009/10/more-details-regarding-smart-devices.html"]
author = "peter"
date = "2009-10-19T21:16:00Z"
layout = "post"
[taxonomies]
tags = ["hdmi", "MIDs", "smartv5", "smartv7"]
categories = ["hardware"]
+++

This evening, after having written down that I was liking the new SmartV5 MID, which isn't yet available in Europe, although it might be in mainland china, back then assuming it was powered by an ARM Cortex A8 based CPU/SoC, i did some research I should have better done before.
<!-- more -->

As it turns out, these devices won't most likely be on par in terms of application speed with today's offerings like the Nokia N900. 

The SoC being used by Smart Devices for their new offerings is most likely (almost certainly in fact, but not confirmed by Smart Devices) the Telechips TCC8900, an ARM11 based design enhanced with some additional circuits to be able to decode 1080p and encode 720p video, mostly used in PMPs.
This means that one cannot expect outstanding application performance from the new MIDs - but after all, this does not mean that they are &#8220;bad&#8221; deals - as the HDMI output makes them (at least this seems very likely after another visit to Smart Devices' website) usable for presentations - as long as your (company's) projector supports HDMI. Compared to the overall liked (mostly due to their low prices) predecessors, the Smart Q5 and the Smart Q7, this new devices will be better at multitasking (and be it multi-tabbing while surfing) - 256MB Ram is not that much, but still twice as much as the 128MB the former devices offered, and as the just mentioned memory will be a lot faster (DDR2@330MHz on the V* compared to DDR@133MHz on the Q*) the overall performance can be expected to be better.

__Sources:__

* <a href="http://www.translate.google.de/translate?prev=hp&amp;hl=de&amp;js=y&amp;u=http://www.smartdevices.com.cn/information/news/200909/24-800.html&amp;sl=zh-CN&amp;tl=en&amp;history_state0=">Comparison between V and Q devices on SmartDevices.com.cn (translated)</a>,
* <a href="http://translate.google.de/translate?hl=de&amp;sl=zh-CN&amp;tl=en&amp;u=http://bbs.gou81.com/viewthread.php%3Ftid%3D28238">Some information on the Telechips TCC8900 (translated)</a>,
* <a href="http://telechips.com/">Telechips.com</a>.
