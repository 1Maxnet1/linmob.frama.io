+++
title = "LinBits 70: Weekly Linux Phone news / media roundup (week 44/45)"
date = "2021-11-10T21:53:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "Manjaro", "SailfishOS", "Nemo Mobile",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_What's new with Linux Phones?_ A new Maui apps release, a Nemo Mobile progress report, Jolla releases Sailfish OS 4.3.0, postmarketOS 21.06 Service Pack 4, Kupfer and more!<!-- more --> _Commentary in italics._ 


### Software releases
* [Numberstation 1.0.0 has been released](https://git.sr.ht/~martijnbraam/numberstation/refs/1.0.0), adding HOTP support and thus being "feature complete"! _Amazing!_
* postmarketOS [have released the fourth and last service pack release](https://postmarketos.org/blog/2021/11/08/v21.06.4-release/) for their 21.06 stable branch, delivering multiple important improvements for usability (Phosh 0.14) and telephony (eg25-manager 0.4.1).
* Manjaro have released [Phosh Beta 18](https://github.com/manjaro-pinephone/phosh/releases/tag/beta18), featuring a few new releases and more polish – an updated changelog post has not yet been posted to Manjaro's forums. 
* [Sailfish OS 4.3.0 "Suomenlinna" has been released](https://blog.jolla.com/suomenlinna/). _No longer for Early Access users!_

### Worth noting
* Martijn Braam made a nice [postmarketOS Cheatsheet](https://www.reddit.com/r/PINE64official/comments/qpllh2/a_postmarketos_cheatsheet_i_made/).
* Work on [enabling suspend for better battery life](https://source.puri.sm/Librem5/librem5-base/-/merge_requests/279) for the Librem 5 has started!
* Mobian have shifted [from nightly to weekly images](https://wiki.mobian-project.org/doku.php?id=install-linux&rev=1636461635&do=diff).
### Worth reading

#### Distros
* TuxPhones: [Kupfer is a postmarketOS-like Arch Linux spin for phones](https://tuxphones.com/kupfer-arch-linux-distro-spin-linux-phones/). _I came across this, but DDG did not feature any useful results - thanks for posting this!_
+ TuxPhones: [postmarketOS + mainline for the OnePlus 5/5T!](https://tuxphones.com/postmarketos-mainline-for-the-oneplus-5-5t/). _Great post by Caleb!_

#### Software progress
* Phoronix: [Linux 5.16 Arm SoC Changes Bring-Up The Snapdragon 690, Other Hardware](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.16-Arm-SoC-Platform).
* This Week in GNOME: [#17 Hourly Backups](https://thisweek.gnome.org/posts/2021/11/twig-17/).
* MauiKit.org: [Maui 2.1.0 Release](https://mauikit.org/blog/maui-2-1-0-release/). _Great progress!_
* Claudio Cambra: [Kalendar is out! — Kalendar devlog 21](https://claudiocambra.com/2021/11/06/kalendar-is-launching-its-beta-kalendar-devlog-21/)

#### PinePhone Pro development progress
* xnux.eu log: [Pinephone Pro – USB, Type-C, OTG, DP-Alt mode, Charger,…](https://xnux.eu/log/#052).
* xnux.eu log: [Pinephone Pro – USB, Type-C, OTG, DP-Alt mode, Charger,… – some success :)](https://xnux.eu/log/#053). _Megi is making progress with regards to PinePhone Pro development!_

#### Nemo Mobile
* Jozef Mlich: [Nemomobile in November/2021](https://blog.mlich.cz/2021/11/nemomobile-in-november-2021/)

#### Other Roundups
* Liliputing: [Linux Smartphone News Roundup: New postmarketOS, Manjaro, Nemo, and Sailfish OS builds, Phosh 0.14, and more](https://liliputing.com/2021/11/linux-smartphone-news-roundup-new-postmarketos-manjaro-nemo-and-sailfish-os-builds-phosh-0-14-and-more.html). _Great round-up by Brad!_

### Worth listening
* Cyberdeck Users Weekly: [Linux on mobile with linmob](https://anchor.fm/futurepaul/episodes/Linux-on-mobile-with-Linmob-e1a0o3k). _Thanks again Paul for having me! Apologies for getting a few things wrong early in the morning and being a bit slow – I normally don't confuse libhybris and halium. I hope you like it anyway!_

### Worth watching
#### Tutorials
* Avisando: [PinePhone - How to update Manjaro Phosh without PC or data loss](https://www.youtube.com/watch?v=-sUGxL46Zyk). _If you ever needed a guide for Pamac, this is it!_

#### PinePhone Pro Progress
* sadfwesv: [i3wm on Pinephone Pro in convergence mode (output to external display)](https://www.youtube.com/watch?v=PTMXgPoylJA).

#### Software Defined Radio
* cemaxecutor: [DragonOS Focal w/ PinePhone + SparrowWiFi Wireless Survey (DragonPhone, HackRF, Ubertooth)](https://www.youtube.com/watch?v=aFVZawlCpoQ). 

#### Ubuntu Touch
* CyberPunked: [Ubuntu Touch - OnePlus 6 / enchilada (2021-01-07)](https://www.youtube.com/watch?v=7y3DWyB5gN4). _Nice!_

#### JingPad
* PizzaLovingNerd: [JingPad Initial Impressions - The Linux iPad](https://www.youtube.com/watch?v=CCwkrtG6B28).	

#### Shorts
* NOT A FBI Honey pot: [Linux user .....finally got the camera working on the #pinephone](https://www.youtube.com/watch?v=L8r71Jjxu8s).
* NOT A FBI Honey pot: [Linux user gets bored so he installed mobian.....](https://www.youtube.com/watch?v=xCjLBZF2FQ0).
* Сергей Чуплыгин: [Volume control directly pulseaudio without nemo modules](https://www.youtube.com/shorts/te1pHRW5aHI). _Nice Nemo Mobile progress!_
#### If you are really bored ...
* Twinntech: [Librem 5](https://www.youtube.com/watch?v=cRBg-KGeD_I). _It's a rough watch. You've been warned!_

### Stuff I did

#### Content
* I finally managed to finish that [setup post](https://linmob.net/my-linux-phone-setup-november-2021/)!
* I also updated [that scaling post](https://linmob.net/pinephone-setup-scaling-in-phosh/).


#### Random
* Void Linux is not as fun as I hoped it to be, especially not with musl.

#### LINMOBapps and Linux Phone Apps

Activity has continued on the low level it reached last week. Cuperino added one app,
* [QPrompt](https://github.com/Cuperino/QPrompt),  a teleprompter software. _Thanks for the Merge Request!_

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
