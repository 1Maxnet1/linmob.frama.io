+++
title = "LinBits 23: Weekly Linux Phone news / media roundup (week 50)"
aliases = ["2020/12/13/linbits23-weekly-linux-phone-news-week50.html", "linbits23"]
author = "Peter"
date = "2020-12-13T19:20:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Mobian", "PineEye", "Phosh"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Phosh 0.7.0, PineEye and more. _Commentary in italics._
<!-- more -->

### Software releases

* [Phosh 0.7.0  has been released](https://social.librem.one/@agx/105357240877980866), adding automatic media mounting, initial support for background XML, handling of per app `show-banners` notification setting and fixes for swipe window closing. [Read the full changelog for more details](https://source.puri.sm/Librem5/phosh/-/releases/v0.7.0).

### Worth reading:

* ThatGeoGuy: [Librem 5 Evergreen vs. Pinephone (Part 1 of ???)](https://thatgeoguy.ca/blog/2020/12/06/Librem-5-Evergreen-vs-Pinephone/). _A comparison article you might want to read, although it leaves some questions unanswered._
* openSUSE news: [Advancing openSUSE Images for The PinePhone](https://news.opensuse.org/2020/12/08/advancing-opensuse-images-for-the-pinephone/). _Nice article on slem.os._ 
* Open VoIP alliance: [Pinephone: Open ecosystem & hardware](https://openvoipalliance.org/blog/quick-guide-pinephone/).
* LinuxSmartphones: [PineEye is a DIY thermal camera add-on for the PinePhone](https://linuxsmartphones.com/pineeye-is-a-diy-thermal-camera-add-on-for-the-pinephone/). _PineEye is a cool project._
* Purism: [Preventing Fragmentation with the Librem 5](https://puri.sm/posts/preventing-fragmentation-with-the-librem-5/).

### Worth watching:

* Mrwhosetheboss: [Are Linux Smartphones about to KILL Android?](https://www.youtube.com/watch?v=GSudn0qB6u0). _This video has a click-baity title and I don't agree with much of it, but it's always interesting to get opinions from those who are not a part of the Linux bubble._
* Purism: [Convergent App Development](https://puri.sm/posts/convergent-app-development/). _Nice video!_
* Gardiner Bryant: [You're WRONG about Linux phones](https://www.youtube.com/watch?v=z29aJCTn-mY). _I would have preferred to see more of the Phones, but still, nice video!_
* GeoTechLand: [Ubuntu Touch On Pinephone: Calls & Text](https://tilvids.com/videos/watch/e9b0ba51-1450-4e8c-b661-1a962b2f7697).
* HomebrewXS: [Gnome Shell on the PinePhone](https://www.youtube.com/watch?v=Qy2Kn3EdYCo).
* PanzerSajt: [Mobian on PinePhone pairing AirPods Pro](https://www.youtube.com/watch?v=xULPYK2fKvM). 
* Pedro Francisco: [Vistazo al PinePhone Manjaro](https://video.hardlimit.com/videos/watch/956b9505-b357-4d8d-a4b5-b1e01e86a2d0), _For those who can understand Spanish, I thought. [As Blort&trade; pointed out on Mastodon](https://social.tchncs.de/@Blort/105375808436203487), there's not much Spanish in it, but it's a nice walkthrough._
* Calling All Platforms Tech: [PinePhone Unboxing](https://www.youtube.com/watch?v=EMg3YvBTm6s).
* Kip: [Ubuntu Touch vs SailfishOS side-by-side on the F(x)tec Pro1 keyboard slider phone](https://www.youtube.com/watch?v=rgxrJj37xOs). _There has to be a video every then and now that's not about the PinePhone or the Librem 5._

### Stuff I did

No videos, no articles, no time.



