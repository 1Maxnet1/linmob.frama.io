+++
title = "Openmoko releases WikiReader - early thoughts"
aliases = ["2009/10/openmoko-releases-wikireader-early.html"]
date = "2009-10-16T13:52:00Z"
[taxonomies]
tags = ["openmoko", "WikiReader",]
categories = ["hardware", "impressions"]
[extra]
author = "Peter"
+++

Openmokos Project B is out, in fact it is since Tuesday. It is a WikiReader, a device you use to read Wikipedia articles on it. Looking at the reception of device, it's been overall mixed.
<!-- mpre -->
Technology enthusiasts were often sceptical, arguing that they would not need such a device, being able to read the Wikipedia everywhere on their smartphone&mdash;and of course they are right, they might not need it, but the question is whether tech enthusiasts are the targeted audience of this rather simple device.

<a href="image_thumb5.png"><img style="margin:10px 0 10px 10px;text-align:center;float:right;cursor:pointer;cursor:hand;width: 320px;height: 320px" src="wikireader.png" border="0" alt="Promotional image of the Openmoko WikiReader" /></a>

In fact have to say that I don't think they are not, and thus I have to conclude that I am not the kind of guy this device is aimed at.

Nonetheless I like the device&mdash;on the one hand I always like highly integrated device, being a fan of mobile computing&mdash;but as much as I like those feature monsters, I like simplicity.

The WikiReader appeals to me, because it is so dead simple (well, if it is, I can't say yet, as I didn't order one yet&mdash;I will once it is available in Europe). Now what do I like about this device? First of all, I like the design, which I'd call beautiful&mdash;if the build quality is reasonable, this device appears to be charming. I like the fact that the device uses microSD as storage&mdash;there is no USB connector, just the microSD&mdash;so upgrades will be a matter of being able to cut and paste. AAA batteries seem to be a good choice to me as well, as the device has to be very power efficient&mdash;they state that it might last for a year on these, figuring a 15 minutes daily usage. This is great, because today's feature monsters always need the wall charger when you need them to look something up.

And last but not least: I love the fact that the device has no wireless connectivity.

Let me try to explain why: Let's start with my T-Mobile G1 (HTC Dream), which is a device certainly much more capable than this little WikiReader. But even though I love it's connectivity options, I have to state that from time to time they really annoy me, exspecially since I have been using <a href="http://www.fbreader.org/FBReaderJ/">FBReaderJ</a> to read some ebooks grabbed at <a href="http://www.feedbooks.com/">feedbooks</a>&mdash;always when I happen to read a really thrilling part of a novel, someone happens to call me, or the LED starts blinking because I got some new mail. Or, what might happen as well, I might get distracted by feeling urged to write a mail, Google this or that ... 

Besides of receiving phone calls, interruptions like this won't happen using a simple device like the WikiReader&mdash;but you can switch your phone off (I could as well&#8230; ;) ).

I could go on like this for ages, pointing out that I like the advertising and the design of the WikiReader website&mdash;but I don't think that this would be worth reading.

And by the way: According to <a href="http://twitter.com/wikireader/status/4905700273">WikiReaders twitter account</a>, Amazon is already sold out of WikiReaders&mdash;assuming they had more than, say 20 in stock, this sounds like the beginning of a good story. 

Being sold and developed by a company which has the word &#8220;open&#8221; in its name, you can grab the sources <a href="http://github.com/wikireader/">here</a>.
