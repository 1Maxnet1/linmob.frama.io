+++
title = "GeeksPhone \"ZERO\" to be released at MWC"
aliases = ["2011/02/geeksphone-zero-to-be-released-at-mwc.html"]
date = "2011-02-07T16:38:00Z"
[taxonomies]
tags = ["Adreno 200", "Android", "Android 2.3 Gingerbread", "CyanogenMod", "CyanogenMod 7", "geeksphone", "GeeksPhone ZERO", "HVGA", "MWC2011", "NenaMark"]
categories = ["hardware"]
[extra]
author = "Peter"
+++
<a href="http://www.geeksphone.com/en/">GeeksPhone</a>, a Spain based, minor cellphone manufacturer focusing on open (and thus hackable) Android phones will release the successor to its first device, the &#8220;ONE&#8221;. The new device  has just been teased in a video on YouTube&mdash;which tells us the name of the product; it will be &#8220;ZERO&#8221;; GeeksPhone is believed to come up with a &#8220;TWO&#8221; later (in fact the &#8220;TWO&#8221; should be released in 2010, but was postponed)&mdash;the &#8220;ZERO&#8221; is going to be GeeksPhone's low end solution. 
<!-- more -->
Watch the introductory video:
* mygeeksphone: [Geeksphone ZERO Benchmark Preview](https://www.youtube.com/watch?v=QqxUUqrNy4k).

As one could already guess, the &#8220;ZERO&#8221; is going to be rather small; according to the GeeksPhone forum the device is going to feature a screen equivalent to the one of the HTC Legend, <a href="http://forum.geeksphone.com/index.php?PHPSESSID=abfa4d79a6abe5d960179c966b819bc1&amp;topic=1365.msg23316#msg23316">&#8220;same size, same resolution&#8221;</a> -3,2&#8221;, HVGA. What's interesting is that the device benchmarked shows off the CyanogenMod7 bootscreen (3:11)&mdash;so it might run CM right of the box&mdash;which would be a nice thing for all of you that are like me accustomed to the <a href="http://www.cyanogenmod.com/">CyanogenMod</a> specific additions.

What else does this video tell about the &#8220;ZERO&#8221;? It runs <a href="http://nena.se/">NenaMark</a>, an OpenGL ES 2.0 benchmark quite decently&mdash;~33 frames per second seems to be a nice result to me (<a href="http://www.youtube.com/watch?v=9flN24jXZbI">in fact this is on par with a Desire HD on Froyo running this at WVGA resolution</a>), though one has to be careful with interpreting this result; the rather low screen resolution has to be considered and it remains uncertain which iteration of Android the benchmarked device ran&mdash;Gingerbread (Android 2.3, which was announced to enhance game/graphics performance) is likely, but one can't tell for sure.

The SoC is, as the benchmark tells, a Qualcomm one with Adreno 200 graphics&mdash;and thus the SoC is either a QSD8250 (Snapdragon, ARMv7) or  (much more likely) a MSM7227 (ARMv6)&mdash;so it's none of the latest and greatest, which fits well to the screen size, which suggests once more that the &#8220;ZERO&#8221; (just like the &#8220;ONE&#8221; will be a low to mid end solution.
