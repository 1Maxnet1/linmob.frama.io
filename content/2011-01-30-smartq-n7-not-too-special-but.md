+++
title = "SmartQ N7: Not too special, but..."
aliases = ["2011/01/smartq-n7-not-too-special-but.html"]
author = "peter"
date = "2011-01-30T20:21:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Android 2.1 (Eclair)", "multitouch", "SmartQ N7", "smartv7"]
categories = ["article"]
+++
Today I had yet another look at the website of the chinese manufaturer SmartQ, who are well known for their Linux/Android/CE MID/tablet devices.
<!-- more -->

And, guess what? On the chinese version of their website I found a device that hasn't been covered by all major techblogs yet&mdash;some did, but the really huge ones didn't: <a href="http://translate.google.de/translate?js=n&amp;prev=_t&amp;hl=de&amp;ie=UTF-8&amp;layout=2&amp;eotf=1&amp;sl=zh-CN&amp;tl=en&amp;u=http://www.smartdevices.com.cn/product/N7/">The SmartQ N7</a>.

Being honest I must admit that this doesn't exactly surprise me; the N7 isn't that different from the well known V7 at the first glance, it features the same Telechips TCC 890x 720MHz ARM11 (ARMv6) SoC with 1080p video playback, 256MB Ram. But it is different: The screen has a better resolution, WSVGA instead of WVGA&mdash;and the touchscreen is this time piezoelectric multi touch, which is both quite an improvement.

What' s not so great is the fact, that the device still ships with Android 2.1 (Eclair), Froyo (2.2) is said to be following in March (<a href="http://www.millennius.com.au/store/tablet-pads/millennius-smartq-n7.html">according to an australian shop</a>)&#8230; no information about Gingerbread (2.3) or the too be released Honeycomb 3.0 (which is the first version of Android optimized for tablets).

While I think that the SoC is fast enough if you are not too picky, I doubt that 256MB will be much fun with future iterations of the Android OS&mdash;the N7 may be able to run Honeycomb (if Honeycomb won't be ARMv7 only)&mdash;but it won't be much fun with this amount of RAM.

If you are aware of this, and just want a cheap tablet with multitouch and HDMI, I consider the N7 (depending on its price point) quite an option.
