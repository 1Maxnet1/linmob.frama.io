+++
title = "LinBits 4: Weekly PinePhone news / media roundup (week 31)"
aliases = ["2020/08/02/linbits4-weekly-pinephone-news-week31.html", "linbits4"]
author = "Peter"
date = "2020-08-02T12:00:18Z"
layout = "post"
[taxonomies]
tags = ["UBports", "Ubuntu Touch", "PureOS", "PINE64", "PinePhone", "Librem 5", "Soldering", "Phosh", "Debian", "hardware mods", "Qt apps", "Flutter", "soldering", "postmarketOS"]
categories = ["weekly update"]
+++

*It's sunday, it's Linbits time!*

This is some the best stuff that happened around the PinePhone this week. _Commentary in italics._
<!-- more -->

### Software: releases and improvements
* [New Lune OS image that does boot](https://nitter.net/Splatoon2weird/status/1287831171863216128). _I tried the 107 build ([image directory](http://build.webos-ports.org/luneos-testing/images/pinephone/), video link below), and they released another image since._
* New PureOS images with rotation lock: [announcement](https://nitter.net/AlexRob12252696/status/1289656977233260544), [image download](https://pureos.ironrobin.net/droppy/#/Images/2020-08-01). _According to a short video review (see below), it is really fast._ 
* [Webian Shell experiments](https://nitter.net/webianproject/status/1289133105111072768). _I was completely unaware that [Webian](https://invidious.13ad.de/watch?v=0TZafLis-fE&t=518) exists. It is a "boot into a browser" thing, and is based on Mozilla's boot2gecko project. Interesting!_
* [@silver came up with a faster camera script](https://fedimaker.space/@silver/104606700207167446).

### Worth reading
* Pine64: [Invitation to Play Along](https://www.pine64.org/2020/07/29/invitation-to-play-along/). _Pine64 are looking forward to building hardware accessories, like a keyboard and a gamepad, for the PinePhone and are asking the community for input and suggestions for the design of these things. Huge news for me, as I always loved hardware keyboards on Smartphones!_
* FreeTube: [Pinephone Review: The State of the Linux Smartphone](https://freetube.writeas.com/pinephone-review-the-state-of-the-linux-smartphone). _Yet another review, long and thorough._
* basysKom: [How to run Flutter on an Embedded Device](https://blog.basyskom.com/2020/how-to-run-flutter-on-an-embedded-device/). _If you want to get Flutter apps on your Linux phone, this might be a good starting point._
* BrixIT blog: [Making a backcover extension for the PinePhone](https://blog.brixit.nl/making-a-backcover-extension-for-the-pinephone/). _This is really cool. If you just want to watch the video, find it below._
* GTK Development Blog: [GTK 3.99](https://blog.gtk.org/2020/07/31/gtk-3-99/). _GTK 4 is inching closer and closer._
* Purism: [Dogwood Thermals and Battery Life](https://puri.sm/posts/dogwood-thermals-and-battery-life/). _What will ship first? GTK 4 or the Evergreen batch of the Librem 5? Snarks aside, they made some important progress. Always remember: Hardware is hard._

### Worth listening
Linux Unplugged: [346: Linux Arm Wrestling](https://linuxunplugged.com/364). _Ever wondered why running Linux on ARM is so much more difficult than doing so on x86/amd64? This episode of the excellent 'Linux Unplugged' podcast has answers._


### Worth watching__
* Martijn Braam: [PinePhone postmarketOS CE: Factorytest and installer](https://invidious.13ad.de/watch?v=_knwLbRprY0). _Impressive work, great video._
* Martijn Braam: [Connecting a thermal camera to the PinePhone pogopin expansion port](https://invidious.13ad.de/watch?v=lFsQpd0bLTY). _Blog post is linked above._
* Howto: Privacy and Infosec: [Howto: Make PinePhone change WiFi MAC Adress at every boot](https://invidious.13ad.de/watch?v=1j-AtFtsqH4). _If you need more privacy, look at this._
* Howto: Privacy and Infosec: [PureOS Is Snappy On Pinephone w/Podcast Player Demo](https://invidious.13ad.de/watch?v=Z_Cuhuu2xwI). _Looks like PureOS has developed nicely and I should take another look at it. He is using a ton of not quite optimized desktop applications there. Really, use Gnome Podcasts not GPodder on your PinePhone with Phosh. I bet most of these don't come preinstalled on PureOS._
* Marius Gripsgård: [GPU accelerated morph browser on the PinePhone!](https://invidious.13ad.de/watch?v=OARRK6bEgiE). _Looks nice, but when will it be available for everyone?_ 
* UBports: [Ubuntu Touch Q&A 81](https://invidious.13ad.de/watch?v=0TZafLis-fE). _Teleports, the UBports client works now with the PinePhone keyboard ([8:38](https://invidious.13ad.de/watch?v=0TZafLis-fE&t=518)), PinePhone News ([16:30](https://invidious.13ad.de/watch?v=0TZafLis-fE&t=990)), and the actual Q&A part is interesting, too._

### Stuff I did
[I shot a short video of LuneOS](https://invidious.13ad.de/watch?v=IQB1QACgZOU), showing off their build 107 and old my Palm Pre Plus. I later tried to change the Kernel (LuneOS is still on 5.5) to Megi's Kernel, but failed on my first attempt. 

Three _#DailyDriverChallenge_ posts were released and one related video on running Qt apps on Phosh. Also, I started playing with Ubuntu Touch again (from SD card), maybe more on that next week.
