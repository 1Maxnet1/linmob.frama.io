+++
title = "Initial thoughts on the PinePhone Pro"
date = "2021-10-15T14:30:00Z"
draft = false
updated = "2022-03-19T12:38:00Z"
tags = ["PinePhone", "Hardware", "PinePhone Pro", "Plasma Mobile", "RK3399",]
categories = ["hardware"]
[extra]
update_note = "2022-03-19: Clarified front camera section even more. 2022-02-03: Front camera change; 2021-10-20: Clarified the section on booting from SD card. Added an additional note to Megi's impressions, fixed a few important typos."
author = "Peter"
+++

Today, PINE64 announced their second phone, the PinePhone Pro. Now what to make of this? What is it? Is it worth the USD 400 price tag? How does it compare to existing Linux Phones? <!-- more -->

### What is a PinePhone Pro?

Going by the name, this is a PinePhone for Professionals, whatever _"professionals"_ might mean in the realm of Linux Smartphones. I could go on, but since we have the specifications, let me just list them here and compare them to the basic PinePhone[^1]:

<table border="0" cellspacing="0" cellpadding="0" class="ta1">
  <colgroup><col width="215"/><col width="877"/><col width="823"/></colgroup>
  <thead>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce1"><strong>Spec</strong></td>
    <td style="text-align:left;width:20.078cm; " class="ce1"><strong>PinePhone Pro</strong></td>
    <td style="text-align:left;width:20.078cm; " class="ce1"><strong>PinePhone</strong></td>
  </tr>
  </thead>
  <tbody>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>System on Chip (SoC) </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Rockchip RK3399s (modified (binned &amp; voltage locked) RK3399) with 2x A72 and 4x A53 CPU cores @ 1.5GHz </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>AllWinner A64 4 x ARM Cortex A53 cores @ 1.152 Ghz</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>GPU </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>ARM Mali T860 4x core GPU @ 500Mhz </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>ARM Mali 400 MP2 GPU</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>RAM </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>4GB LPDDR4 @ 800MHz </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>2GB / 3GB LPDDR3 RAM</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Storage </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>128GB eMMC flash storage </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>16GB / 32GB eMMC</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>LCD panel </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>1440 x 720 in-cell IPS with Gorilla Glass 4TM </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>5.95″ LCD 1440×720, 18:9 aspect ratio (hardened glass)</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Main camera</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>13MP Sony IMX258 main camera with Gorilla Glass 4TM protective layer, LED Flash </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>5MP Omnivision OV5640, LED Flash</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Selfie camera</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p><del>5MP OmniVision OV5640</del> <ins>8 MP OmniVision OV8858</ins> front-facing camera </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>2MP GalaxyCore GC2145 f/2.8, 1/5″</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Modem &amp; GPS </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Quectel EG25-G - global GSM and CDMA bands GPS, GPS-A, GLONASS </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Quectel EG25-G - global GSM and CDMA bands GPS, GPS-A, GLONASS </p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>WiFi/ BT </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>AMPAK AP6255 WiFi 11ac + Bluetooth V4.1 </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Realtek RTL8723CS 802.11 b/g/n WiFi with Bluetooth 4.0</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>I/O </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Micro SD slot</li><li>Pogo-pins (OG PinePhone compatible)</li><li>USB-C- power, data (USB 3.0), and DP alt-mode video out</li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Micro SD slot</li><li>Pogo-pins</li><li>USB-C- power, data (USB 2.0), and DP alt-mode video out</li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Sensors </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Accelerator</li><li>Gyroscope</li><li>Proximity</li><li>Compass</li><li>Ambient light</li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Accelerator</li><li>Gyroscope</li><li>Proximity</li><li>Compass</li><li>Ambient light</li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Privacy hardware switches </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Cameras</li><li>Microphone</li><li>WiFi and Bluetooth</li><li>LTE modem (including GPS)</li><li>Headphones (to enable UART output)</li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Cameras</li><li>Microphone</li><li>WiFi and Bluetooth</li><li>LTE modem (including GPS)</li><li>Headphones (to enable UART output)</li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>External buttons</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Volume up / down rocker</li><li>Power ON/ OFF </li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Volume up / down rocker</li><li>Power ON/ OFF </li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Audio</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Headset speaker</li><li>Audio jack</li><li>Loud Speaker</li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Headset speaker</li><li>Audio jack</li><li>Loud Speaker</li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Other</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Flash / Torch</li><li>Vibration motor</li><li>Status LED</li><li>UART via audio jack</li></ul></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><ul><li>Flash / Torch</li><li>Vibration motor</li><li>Status LED</li><li>UART via audio jack</li></ul></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Battery</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Samsung J7 form-factor 3000mAh </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Samsung J7 form-factor 3000mAh </p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Charging</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>5V 3A (15W) Quick Charge - USB Power Delivery specification </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>5V 3A (15W) Quick Charge - USB Power Delivery specification </p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Dimensions</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>160.8 x 76.6 x 11.1mm </p></td><td style="text-align:left;width:20.078cm; " class="Default"><p>160.5 x 76.6 x 9.2mm</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Weight</p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>Approx. 215g </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>180-200 g</p></td>
  </tr>
  <tr class="ro1">
    <td style="text-align:left;width:4.93cm; " class="ce2"><p>Price </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>$399 (not inc. shipping &amp; import tax) </p></td>
    <td style="text-align:left;width:20.078cm; " class="Default"><p>$149 / $ 199 (not inc. shipping &amp; import tax)</p></td>
  </tr>
  </tbody>
</table>

### Now what to make of this?

#### SoC and GPU

Let's start with the SoC. The PinePhone's AllWinner A64 will never be fast. Software optimization has improved reliability and speed of the PinePhone in the ~16 months I've had it quite significantly, and there are further gains to come when GStreamer 1.20 will be released and lands in distributions, but things like starting a heavy application like a web browser always take a bit. Extensive multitasking is no fun easier, things are going to crawl to a halt. 

This is where the Rockchip RK3399s with it's hexacore design, featuring to Cortex A72 performance cores is going to shine. Now, keep in mind: This chip is not as fast as the Qualcomm Snapdragon 845 that has good mainline support in devices like the OnePlus 6(T), but it's a significant step up over the AllWinner A64 (and also the NXP i.MX8M in the Librem 5). I can say this on good authority, having used RK3399/OP1 powered devices since late 2017 to this day, and the mainline Linux support is really in a good state now, even for the GPU. While the PinePhone Pro uses an underclocked version of this chip for thermal reasons, PINE64 state that this underclocking does not have a significant impact on performance (only 20% less compared to the PineBook Pro). 

Clearly, this is not the highest performance ARM chip imaginable, and it may be a bit old. In fact, it was announced in January 2015, started shipping in consumer devices in February 2017, and is manufactured in a 28nm HKMG process. 

But: This is not what matters. What matters, is that the product is available in quantities that work for PINE64 (which can't be said for the Snapdragon 845, just look up what happened to [F(x)tec and their originally Snapdragon 835 based Pro1X](https://liliputing.com/2021/02/fxtec-pro1-x-will-ship-in-august-with-a-snapdragon-662-processor-rather-than-march-with-an-sd835-chip.html)) and that is has good mainline support (which can't be said for the newer RK3566 chipset yet).

#### RAM and Storage

The orginal PinePhone is available in 2GB and 3GB LPDDR3 RAM flavours, with 3GB RAM being the maximum amount of RAM the AllWinner A64 can handle. This has been bumped up to 4GB of LPDDR4 RAM, which is the maximum the RK3399 can handle. 

While it would be great to have more, I doubt it would be possible without worse trade-offs at this point in time for PINE64 as I tried to explain above.

So, that's 1 GB more RAM, which still is something to look forward to. Additionally, eMMC storage concerns are going to be less of a concern with 128GB of eMMC storage, which is something I look forward too a lot for [LINMOBapps](http://appl.ist) software try-outs alone.

_One thing to note though:_ <del>I have been told that you won't be able to boot from microSD(HC|XC), as that's an AllWinner speciality, which matches up with what's known about the RockPro 64 and PineBook Pro. However, I expect that this something that might change with time and some community boot loader developments.</del> This apparently is causing confusion as I wrote this badly. The key thing I meant to write is that PinePhone Pro is likely not as unbrickable as the original PinePhone is, as the SD card does not have the first boot priority over eMMC when an eMMC is present (which is the case with the PinePhone Pro). This can be changed in software (e.g. via u-boot), so that the Phone boots from SD card if possible, but if something goes wrong with that bootloader you may need to deal with test points to fix your phone. In practice, this theoretical bricking might never happen to you and thus make no difference at all.

#### Wireless Connectivity

__Cellular/LTE:__ It's the same Quectel EG25-G modem we know and love from the PinePhone. This is good news, as a lot about how to make this chip work has been already learned and should apply to the PinePhone Pro, too, including all the work on replacing the original firmware.

__Wifi and Bluetooth:__ No more Realtek, yay! Also, 5GHz WiFi 5/AC is now a thing, so that you can take advantage for your faster home networks. But what is that Ampak AP6255 Wifi SDIO hardware? From what I could research on the web, this is based on the Broadcom BCM 43455 chip. So if you were hoping to rid yourselves of a proprietary firmware blob here: That's not going to happen. 

PINE64 could have probably gone with the BL602/BL604 chip, and I suppose they might eventually offer a PinePhone Pro with that alternative WiFi/BT chip once the FOSS Firmware for that is developed enough for the people that are willing to trade 5 GHz WiFi and Bluetooth for one less blob.

All in all, this is better than what the PinePhone has, staying true to the Pro moniker.


#### Cameras

<del>While the front camera is well known, as it's the back camera of the OG PinePhone,</del><ins>The PinePhone Pro's front camera was changed to a different camera after the original announcement, it's an 8MP OmniVision OV8858 which does not have a driver in the Linux kernel yet, so ... in terms of easy hardware enablement, this is definitely not a welcome change.</ins> The Sony IMX258 back camera is apparently a common sensor in [low- to mid-range Android smartphones](https://www.kimovil.com/en/list-smartphones-by-lens-model/sony-imx258). It's definitely a step up from the PinePhone, but you know how it is: Smartphone cameras are all about software in general and especially in mobile Linux - don't expect to be able to have an excellent point and shoot experience with this camera after unboxing your Enthusiast Linux Phone. It might take a while.

#### The Rest

__Sensors, Display, Measurements.__ Sensors have been kept the same and accessory compatibility has been maintained. Because of thermal optimization (I guess), and a new, higher quality but same resolution display, the PinePhone Pro is 2 millimeters thicker, which totally fine by me. 

__Software.__ Also, just like the PinePhone Beta Edition, the PinePhone Pro is going to ship with Manjaro Plasma Mobile too. With this faster hardware and Plasma Mobile switching over to Modem Manager this is going to be a pleasant default experience. PINE64 expect most of the PinePhones community distributions getting ported to the to PinePhone Pro, and I do believe that too.

{{ nonsquaregallery() }}
<center><em>It looks just like a PinePhone, surprising, huh? (Images courtesy of PINE64)</em></center>



### What we don't know yet

__Battery life.__ I guess that the RK3399s is going to be better in active use than the AllWinner A64, and hope that together with the new, higher quality display we're going to see a better screen on, in use battery life. Rockchip implemented a suspend state, so we'll have a CRUST-like feature here too, which should lead to good "stand-by" battery life, which really matters in my experience with the PinePhone (which has this) and the Librem 5 (which does not have it yet, and thus gets dusty).

### Conclusions

Like with other products, Pro, which once was short for "professional" now means "it's got better specs". This same is true here. Overall, for double the price (before shipping and taxes) you're getting an enthusiast Linux Phone product that may not be as much better (in terms of specs) as you may have hoped, but better enough that it makes a major difference in daily use and is totally worth it.[^2]

_I will definitely order a PinePhone Pro, although I won't get one of the Developer Units and will wait for wider availability._

### Read more
* PINE64 has a product page for the [PinePhone Pro](https://www.pine64.org/pinephonepro/),
* also read their [October Community Update](https://www.pine64.org/2021/10/15/october-update-introducing-the-pinephone-pro/),
* Brad Linder has a nice article at [liliputing.com](https://liliputing.com/2021/10/pinephone-pro-is-a-faster-linux-smartphone-for-399.html),
* Martijn Braam has had the device for a while and made an [awesome video](https://spacepub.space/w/7BAJhAaBKwfhMuUVaMWGAd) (which is also on [YouTube](https://www.youtube.com/watch?v=pCxDcMdr_fo)),
* Megi has written up [his impressions](https://xnux.eu/log/#047). _This makes me want to eat my hat with regard to my hopes for better battery life in active use &mdash; but we'll see. Pre-production hardware and combined with early software may not show the true picture here._ **Additional note**: Lukasz Erecinski of PINE64 contacted me to point out that Megi's measurements were done on hardware with stock RK3399 chips, that are neither binned nor voltage locked and underclocked and thus do not represent later production hardware or even the later developer units. _So please take these measurements with a lot of salt!_

[^1]: For time and layouting reasons, I only list the PinePhone here as comparison. I hope to publish a more broad Linux Phone comparison table with an overall overhaul of my resources page before November.

[^2]: One might argue that used Snapdragon 845 devices are both faster and cheaper, which is a fair point. But: Changing the battery on the devices for convenience or long term use is way more difficult with these devices and most importantly, they don't support convergence &mdash; you can't plug them into a larger display like you can with the PinePhone Pro.
