+++
title = "OLPC and co"
aliases = ["2009/01/31/olpc-and-co"]
author = "peter"
date = "2009-01-31T12:29:00Z"
layout = "post"
[taxonomies]
tags = ["Classmate", "developing world", "OLPC", "OLPC-XO2", "OPLC XO-1", "VIA"]
categories = ["hardware", "personal",]
+++
As you might know when you've been reading this blog already, and as you know when you followed it, all this here is pretty much about gadgets. Of course, I mostly buy used units, when I buy mobile devices, due to the fact that I don't have that much money to be a first adopter (and due to the fact that this is all about linux), but I have to admit that these gadgets aren't all.

There is a lot of poverty in this world, a lot of hunger, many people starving, not so many here in Europe and not many in northern America, but if you have a look at the rest of the world, there are in fact many people that live in extreme poverty. This is a situation one often can't imagine.<br />And this is a situation, which shouldn't stay, even if this can mean, that the rich countries become a little bit less rich. 

There are lots projects and organizations, which try to help the poor people. Some help with deseases, others with food or cloth, and then there are organizations like the <a href="http://www.laptop.org">&#8220;One Laptop Per Child&#8221;</a> project, which trys to bring knowledge to the children of these poor countrys to enable them to build a better future. I have been following the evolution of this project since its beginnings and it has my sympathy. And the way the project has gone to now wasn't always easy, I have read people, that really were to stupid to think and said that it would be better to give these children books, as if there were no ebooks. Then there is Intel, who haven't behaved well regarding OLPC, and many others critizising the project, exspecially after Nicholas Negropontes decision, to ship some units with Windows XP. 

Those who really read the crap I write here, might remember that I had my <a href="http://linmob.blogspot.com/2008/03/cebit-2k8-short-coverage.html">hands on the OLPC XO on last years CeBIT</a>. It is a nice device, and if it was sold to normal people at a price some ten Euros above manufacturing cost, I think it would have found more buyers than it did with the &#8220;Give 1 Get 1&#8221; program, regarding the success of the ASUS eeePC (and the follow up devices).

For some time there have been mockups of the OLPC XO2, the successor if the 100$ laptop (XO), whichs' cost target is 75$, and it will look more like a book, having two touch screens. 

Very recently <a href="http://www.guardian.co.uk/technology/2009/jan/29/nicholas-negroponte-olpc">OLPC founder Nicholas Negroponte announced additionally</a> that it will be openly designed. This may include that they will even use <a href="http://www.openhardware.org/">Open Hardware</a>, but at least it should imply that they will release the design of the device under a free license, such as VIA did with its <a href="http://www.google.de/url?sa=t&amp;source=web&amp;ct=res&amp;cd=1&amp;url=http%3A%2F%2Fwww.viaopenbook.com%2F&amp;ei=CJOESd-hNozY0gWBiNxS&amp;usg=AFQjCNGVouC8np-Fglft3n2pN5xvgrF5iA&amp;sig2=-tFCtc1I0eYEjzYCV1Rh8w">Openbook</a>&mdash;Design and of course the hardware specifications. This could result in other manufacturers making OLPC XO-2 clones for the markets of non-emerging countrys&mdash;definitely a good move.

The last thing that I want to mention is that there are competitors to OLPC, the most interesting besides <a href="http://en.wikipedia.org/wiki/Classmate_PC">Intels Classmate</a> offer is an Indian initiative which aims to build a 10$ laptop&mdash;which sounds pretty unbelievable.<br /><a href="http://timesofindia.indiatimes.com/India/Rs_500-laptop_display_on_Feb_3/articleshow/4049914.cms">But we will see it pretty soon&#8230;</a>

P.S.: I'd also recommend to <a href="http://www.xconomy.com/boston/2009/01/29/olpc-20-after-layoffs-one-laptop-foundation-reboots-with-new-focus-and-big-plans/">read this..</a>
