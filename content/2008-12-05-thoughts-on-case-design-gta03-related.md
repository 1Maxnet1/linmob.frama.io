+++
title = "Thoughts on case design (GTA03 related)"
aliases = ["2008/12/thoughts-on-case-design-gta03-related.html"]
date = "2008-12-05T13:20:00Z"
[taxonomies]
tags = ["GTA03", "hardware design", "openmoko", "platforms"]
categories = ["commentary", "hardware"]
[extra]
author = "peter"
+++
After I mentioned the Openmoko GTA03 in my last post, I had to watch out, what this device would look like. I didn't find much, no pictures but an old one from an <a href="http://www.youtube.com/watch?v=wsnFdexq7IY">old video</a> I remembered, though I have been searching a short while, but after a while, I decided think of a case which I would like.
<!-- more -->

First of all, a case has to match usage scenarios. Thinking of the case design of GTA01 and GTA02 (&#8220;FreeRunner&#8221;), they were in some cases horrible: Touchscreen devices, but no place to put your touchpen&mdash;in a way you could call this &#8220;mission failed&#8221;.

Now, let's think about what the GTA03 is. It is a device, which will be mainly bought by hackers and people, that need a open base for a hobby or scientific or whatever project.
Almost nobody would recommend an Openmoko phone to somebody that just needs a phone for occasionally calls and SMS. And nobody would recommend this phone for his grandma.

Anyway, as the software stacks become not just more and more numerous, but more stable and reliable, Openmoko's devices become more interesting for (advanced) users that like the idea of free software. The rounded design of GTA01 and GTA02 has been critized, so it should be kept and replaced by one with harder corners.

I found a <a href="http://lists.openmoko.org/nabble.html#nabble-td1376109i20">mailing list discussion</a> in which someone proposed some options: 

1. touchscreen without keys
2. non touchscreen qwerty
3. touchscreen with qwerty

I would opt for option one, as I think that a keyboard costs a lot, means (especially if the phone isn't candy bar but slider) much more complex engineering and localization. No matter how much I love my Samsung SGH-i780s keyboard&mdash;it isn't worth it. If you consider the usage scenarios, a keyboard might be nice, nonetheless. Writing (not only, but mainly) on the walk is much easier with a physical keyboard. And if consider additionally, that Openmoko's devices are claimed to be very little computers, a keyboard makes this much more real (keep in mind that GTA03 will contain a more powerful SoC). But does this mean, that the keyboard has to be there all the time?

No, as GTA03 will certainly feature (as GTA01 and GTA02 do) an USB port, which isn't just client, but host, too, I'd say an openly documented expansion system which would make it possible to plug additional hardware directly to the GTA03 without making mechanical forces harm the USB port would be THE solution. Imagine a little keyboard you just plug to your GTA03&mdash;of course, this might mean to carry one additional thing (but it doesn't have to be like that, imagine a clam-shell like concept (you could do that with this funny hole in the released Openmoko devices as well, btw)&mdash;wouldn't it be great?
