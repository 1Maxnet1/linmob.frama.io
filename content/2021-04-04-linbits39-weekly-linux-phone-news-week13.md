+++
title = "LinBits 39: Weekly Linux Phone news / media roundup (week 13)"
aliases = ["2021/04/04/linbits39-weekly-linux-phone-news-week13.html", "linbits39"]
author = "Peter"
date = "2021-04-04T21:42:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "postmarketOS", "Phosh", "Sxmo", "Nemo Mobile", "DanctNIX", "ARMv9", "VNC"]
categories = ["weekly update"]

+++

_It's sunday. Now what happened since last sunday?_

A new postmarketOS beta release, great DanctNIX changes, GTK4, Sxmo and Phosh releases and Nemo Mobile progress! _Commentary in italics._
<!-- more -->


### Software development and releases

* Arch Linux ARM [has seen another release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20210331). _Since then, they've added [another important change and now use eg25-manager as the modem driver](https://fosstodon.org/@danctnix/105992856591405257), which makes the phone part better and more reliable. Also, [siglo, the InfiniTime companion,](https://twitter.com/DanctNIX/status/1378714775627046914) has been packaged._
* postmarketOS beta 21.03 [has been announced and released](https://postmarketos.org/blog/2021/03/31/v21.03-release/). _This beta branch based on Alpine Linux 3.13 is what was originally called stable. You don’t have the frequent updates you get with edge, but also less breakage._
* Phosh 0.10.0 [has been released](https://social.librem.one/@agx/105985306959622087), [full release notes](https://source.puri.sm/Librem5/phosh/-/releases/v0.10.0). _Another great release of Phosh. I had almost zero crashes since phoc 0.7.0 and Phosh now improves on many of its dialogues. Awesome progress!_
* Sxmo [1.4.1](https://lists.sr.ht/~mil/sxmo-announce/%3C20210331184151.elfuxe6gkyisr3h6%40worker.anaproy.lxd%3E) and [1.4.0](https://lists.sr.ht/~mil/sxmo-announce/%3C20210329205326.i4veoh64u6huect6%40worker.anaproy.lxd%3E) have been released, making it more user friendly. _Make sure to also read Brad's post and watch Martijm's video on Sxmo that are linked below._
* Nemo Mobile [have announced](https://twitter.com/neochapay/status/1377955681261793285) and released a [new Manjaro based image](https://img.nemomobile.net/devel/). 

### Worth noting 
* [SIP in Calls is nigh!](https://social.librem.one/@agx/105990302694777491)

#### April Foolery (no spoilers)
* [1](https://fosstodon.org/@danct12/105989230357177642)
* [2](https://mastodon.technology/@kde/105989192547196542)

### Worth reading 
* Anandtech: [Arm Announces Armv9 Architecture: SVE2, Security, and the Next Decade](https://www.anandtech.com/show/16584/arm-announces-armv9-architecture). _ARMv8 was about 64bit, ARMv9 is supposed to be about security. If you me allow me a guess, this will introduce proprietary security components FOSS-heads won't be able to trust &mdash; think Intel Management Engine, but ARM. But performance is supposed to increase notably, which is a good thing._
* Adrien Plazas: [Introducing Libadwaita](https://adrienplazas.com/blog/2021/03/31/introducing-libadwaita.html). _Now, we had libadwaita mentioned before on [LINMOBapps](https://linmobapps.frama.io), but now it's officially announced. I think that this is a great idea that will benefit both, the wider GTK ecosystem and GNOME._
* GTK Development Blog: [GTK 4.2.0](https://blog.gtk.org/2021/03/30/gtk-4-2-0/). _A new GTK release with a new renderer, see [these tweets](https://twitter.com/linmobblog/status/1377218647677136897). With this, it's cross platform support and libadwaita, GTK4 will have a nice future._
* Linux Smartphones: [Sxmo 1.4 released with keyboard, display, text rendering improvements for the lightweight Linux phone UI](https://linuxsmartphones.com/sxmo-1-4-released-with-keyboard-display-text-rendering-improvements-for-the-lightweight-linux-phone-ui/). _Nice improvements. I look forward to trying it._
* TuxPhones: [Tens of Nvidia Tegra devices now run mainline Linux thanks to the "grate" project](https://tuxphones.com/nvidia-tegra-mainline-linux-grate-postmarketos/). _Sustainable device lifetimes despite NVidia. Awesome!_
* Linux Smartphones: [Ubuntu Touch port for the Fairphone 3 running Android 10](https://linuxsmartphones.com/ubuntu-touch-port-for-the-fairphone-3-running-android-10/). _Great to see Ubuntu Touch landing on the Fairphone 3._ 
* UBports: [Ubuntu Touch Q&A 97](https://ubports.com/blog/ubport-blogs-news-1/post/ubuntu-touch-q-a-97-3749). _The blog post and audio version of last weeks Q&A._
* nss.ee: [This blog is now hosted on a GPS/LTE modem](https://nns.ee/blog/2021/04/01/modem-blog.html). _This is on the stock modem firmware of the PinePhone. Remember: The modem has it's own Cortex A7 core, which is nothing to sneeze at._
* FOSS2go: [How to Add a Custom Background in Phosh?](https://foss2go.com/how-to-add-a-custom-background-in-phosh/). _Nice how to!_
* Purism: [Snitching on Phones That Snitch On You](https://puri.sm/posts/snitching-on-phones-that-snitch-on-you/). _While I wish that the Librem 5 was as good at more regular phone use cases, this is an impressive case for Linux Phones._
* Kirsle.net: [VNC Server for Pinephone](https://www.kirsle.net/vnc-server-for-pinephone). _Great overview and how to!_

### Worth listening
* postmarketOS podcast: [#4 EG25-G, v21.03, Why Alpine, mobile-config-firefox](https://cast.postmarketos.org/episode/04-EG25G-v2103-Why-Alpine-mobile-config-firefox/). _Great podcast, give it a listen!_

### Worth watching
* Martijn Braam: [SXMO PinePhone Demo](https://odysee.com/@martijn:b/pinephone-sxmo:3). _Sxmo 1.4 demoed._
* PizzaLovingNerd: [webOS Is Back! (PinePhone OSes: LuneOS)](https://tilvids.com/videos/watch/eb45639f-f7c8-479a-b6e6-c34e2eef4485). _Nice video by PizzaLovingNerd._
* Newbyte: [PinePhone memory clock speed comparison with postmarketOS](https://odysee.com/@Newbyte:7/pp_sd:b). _Interestingly, there's virtually no difference in Firefox startup speed visible between the different RAM clocks._
* E. Matt Armstrong: [PinePhone PostmarketOS Part 1](https://www.youtube.com/watch?v=Tkunw_k3ngw), [2](https://www.youtube.com/watch?v=gBMTBdFfr3M), [3: Apps](https://www.youtube.com/watch?v=gypnrKANvTg), [4: Themes](https://www.youtube.com/watch?v=F9v7BX-wLS0). _A couple videos about postmarketOS Plasma Mobile._
* E. Matt Armstrong: [PinePhone Arch Distro Spin Part 1 Setup and Apps](https://www.youtube.com/watch?v=_njGKn8u3UA). _Nice DanctNIX walkthrough._
* Sk4zZi0uS: [PinePhone Challenge - One Month as a Daily Driver starting now -2021-04-01](https://www.youtube.com/watch?v=XHJ-xXu8c9M). _A bit long, but I am curious how the months is going to go._
* techexplains: [installing full linux on android hardware (samsung galaxy s8) (postmarket os guide)](https://www.youtube.com/watch?v=pGvSQgTp2Y4). _Nice tutorial of a postmarketOS installation._


### Stuff I did

#### Content
None, sorry. 

#### Random
I switched my install over to postmarketOS edge this week, because I finally wanted to experience that EG25-manager goodness, only to see it landing hours later on DanctNIX. I will likely go back eventually, but for now I want to play with postmarketOS a bit more.

#### LINMOBapps

I added four apps to LINMOBapps: Sums, a calculator; Characters, an Emoji picker; Mousai, a Shazam clone; and Tubefeeder, a GTK+Rust YouTube client. Make sure to check them all out! [See here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 
