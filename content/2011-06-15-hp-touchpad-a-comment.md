+++
title = "HP TouchPad - A Comment"
aliases = ["2011/06/15/hp-touchpad-a-comment.html"]
author = "peter"
date = "2011-06-15T10:57:00Z"
layout = "post"
[taxonomies]
categories = ["commentary", "hardware"]
tags = ["HP Touchpad", "HP webOS", "usability", "webOS 3.0"]
+++
Ben (<a href="http://twitter.com/benz145">@benz145</a>) of carrypad.com wrote an article on the TouchPad titled &#8220;Strengths and Weaknesses — A Look at HP’s TouchPad&#8221;, which I really recommend you to read, it's a great article. In fact, you should <a href="http://www.carrypad.com/2011/06/14/strengths-and-weaknessesa-look-at-hps-touchpad/comment-page-1/#comment-28287">head right over and read it now</a>, because what follows is a <i>slightly edited</i>  lengthy comment I wrote on that article:

<!-- more -->

<b>The Hardware:</b> I absolutely agree with Ben here. It is a huge disappointment, that HP didn't opt for more connectivity options. No HDMI really kills some usage cases, especially if you consider that HP wants to sell many TouchPads to businesses. USB OTG / Host would have been nice too, but I don't think it's that crucial. I still don't understand why no tablet manufacturer manages an SD card reader (not speaking of µSD here) into their tablet. Many of us still use SD Cards in our cameras, as they are still cheaper at large sizes / high speeds – especially with a 3G equipped tablet (BTW: Why no 3g on launch, HP?) this is really bad. The manufacturers could easily include such a slot in a manner that the upselling still worked (sticking out SD) – and still it would be appreciated.

<b>The Price:</b> I totally agree with HP, that a too aggressive pricing would have implied that the TouchPad was an inferior product. Setting the exact same price is a huge mistake though, as the TouchPad will be perceived by many as an inferior product. It is heavier (740g) and thicker (13.7mm) and we don't know yet how fast and fluid it will feel: <i>Many customers will notice that it is a first generation product.</i> Add the lack of an video out option and the fewer apps and content deals to it (<i>=&gt; the inferior ecosystem</i>), and you are going to find out that the TouchPad is actually an inferior product. Setting a high price point will not hide this. And we haven't even added in the fact that Apples products are way more hyped than HPs will possibly ever be in the next three years.

<b>Another note on the App situation:</b> One has to keep in mind that webOS 3.0, which is launched with the TouchPad, breaks compatibility with older apps, as the old Mojo apps will only run inside an emulator with a virtual gesture area (<i>=&gt; not quite a breathtaking experience</i>). From what I read in my Twitter timeline, the new development kit called Enyo is great, even though it's not as close to web development as Mojo was. Almost all of the apps I use on my Pre Plus (which I love, btw) will be available in a native flavour on the TouchPads launch day.

Than there is <b>one more thing</b> I want to point out: From what I sneeked from all these videos about the TouchPad I've watched, the experience won't be as great as on a webOS phone. While dumping the gesture area (all ways I can imagine to implement it on a tablet have flaws) is not necessarily a bad thing (as many first time users of the webOS phones didn't understand the concept of it at first, anyway (i am talking about trying it in a shop without anyone telling you how to use it), there is something that I consider a mayor caveat. Apparently, there is no way to swipe from one way to another without going back to card mode, and even worse, to get to card mode, you have to press this one (I think it's capacitive) button, which is always at one point. The Android guys did a better job at that, and the (in terms of user experience) webOS rip off TabletOS/QNX does a better job there, too.

#### Additional comment:

I have to add that I don't see the HP Touchpad as a definitive failure. It has certain features that will differentiate it from other devices, just think of the &#8220;touch to share&#8221; thing. However, the competition will implement these sooner or later (rather sooner), so HP must not rest on its laurels. They have to move forward, if they want to win a market share that makes the investment in webOS a profitable one. Competition is tough, now, and it will become even more fierce, as Google, Apple and Microsoft do not stand still - from what we've seen lately with iOS 5, Ice Cream Sandwhich and Windows 8 they do quite the opposite.
