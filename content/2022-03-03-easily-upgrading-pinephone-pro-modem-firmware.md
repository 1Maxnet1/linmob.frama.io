+++
title = "Easily upgrading the PinePhone (Pro) Modem Firmware"
date = "2022-03-03T19:50:18Z"
updated = "2022-03-11T12:40:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Quectel EG25-G","open source modem firmware",]
categories = ["howto",]
[extra]
author = "Peter"
update_note = "2022-03-11: Added warning. 2022-03-04: Clarified PinePhone Pro install process, added footnote to link to testing results with non-Phosh postmarketOS."
edit_post = true
+++

If you are a PinePhone owner and have not been living under a rock, you will know that there's an [exploitable vulnerability](https://nvd.nist.gov/vuln/detail/CVE-2021-31698) for the PinePhone Pro's Quectel EG-25G firmware that's been supplied with your PINE64 phone.<!-- more --> Also, let's briefly say that aside from not being good at fixing vulnerabilities in a timely manner, Quectel and their modem firmware by default are not really perfect.

### Warning!
<mark>I believed this process was failsafe, but I am saddened to hear that it's not necessarily so – one Braveheart edition PinePhone did not go through the process as intended.</mark>
So: Please only do this, when you know that you are playing with experimental software and _understand_ the [instructions to reinstall the official firmware](https://github.com/Biktorgj/quectel_eg25_recovery).[^0] There's no graphical way to go back, and it's unlikely there'll ever be one, as that would require Quectel to supply its official firmware through the [Linux Vendor Firmware Service](https://fwupd.org/) project.

### The Fix (part 1): The Community Firmware by Biktorgj

Fortunately, there's a road to getting that problem fixed without waiting for Quectel to deliver something. PINE64 Community Member Biktorgj has been working hard on a community firmware – I've written about this, and wrote an [explainer on how to install that firmware](https://linmob.net/flashing-biktorgjs-modem-firmware/) in May of 2021. Installing got simpler since (just run one script), but not quite simple enough for many.

### The Fix for Everybody (part 2): Easy installation by Dylan van Assche

Thankfully, postmarketOS Community Member Dylan van Assche[^1] devised a graphical solution since in a masterful way, utilising and more importantly working with existing projects in the "Linux and Firmware" space. For details, make sure to read his excellent [blog post on the topic](https://dylanvanassche.be/blog/2022/pinephone-modem-upgrade/).


### Caveats

Here's the official stance on installing alternate firmware in the [February 2022 PINE64 Community Update](https://www.pine64.org/2022/02/15/february-update-chat-with-the-machine/) (quoting PINE64 Community Manager Lukasz Erecinski; _emphasis mine_):

<blockquote>The work done by Biktor is incredible as is the effort by Dylan to package the firmware into fwupd [...]. I think that it is clear at this point that, objectively, this is the definitive firmware for PinePhone / PinePhone Pro’s modem – not to mention that it is also the safest. <bold>For legal reasons I need to state the following: 1) we do not encourage users to modify their modem’s firmware nor do we encourage it and 2) altering the firmware on the modem does void device warranty.</bold></blockquote>

### Now: How do I do this?

#### Installing postmarketOS edge on a spare microSD card

_Assuming you're not running postmarketOS with Phosh[^2] anyway:_

In fact, by now, the easiest way to do this still requires some terminal fiddling, sadly. You'll also need a space microSDHC/microSDXC card. The process starts with downloading postmarketOS edge for your PinePhone.

Start by downloading latest PinePhone image from [here](https://images.postmarketos.org/bpo/edge/pine64-pinephone/phosh/) (then non-installer image should do), and write it to that microSD card with your software of choice.[^3]  For now, you can't do this for your [PinePhone Pro, and have to use pmbootstrap](https://wiki.postmarketos.org/wiki/PINE64_PinePhone_Pro_(pine64-pinephonepro)#Installation) which is a worth learning anyway.[^4]

After that, put that microSD card in your phone and boot it up.

After connecting to Wifi, make sure to update your distribution (with GNOME Software, or by using `sudo apk -U upgrade` in the terminal). You should plug in your phone beforehand (you don't want to loose power while upgrades are applied), and this time, just leave it plugged in. If it's been a bigger upgrade, do reboot.

After that, install GNOME Firmware Updater by running `sudo apk add gnome-firmware-updater`.
Having the software installed, connect your phone to a charger[^5], start the "Firmware" app and then hit ...

#### Video time

I won't describe the following word by word, as I've detailed this on video a few days ago:
* [Watch it on TilVids](https://tilvids.com/w/2htMubjLy6Wh6yeX3jaQJa)
* or [YouTube](https://www.youtube.com/watch?v=IsFbVZsQJX4).


After about 5 to 10 Minutes, the firmware is going to be installed on your phone.


#### Alternative: Use DanctNIX

If you are running Arch Linux ARM by Danct12, you can also do this. You'll need
* |the AUR for now](https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/#preparations),
* install `gnome-firmware-git` with your AUR helper of choice (or in the manual "clone from git and use makepkg and get dependencies by hand",
* and you'll need to [improve your polkit handling](https://blog.timo.page/authorizing-administrative-tasks-as-non-root-user-in-gnome) to do this.

After that, it should just work just as it does on postmarketOS - you'll find GNOME Firmware Updater on the "All apps" app launcher

### What's left to do?

Installing this modem firmware will, likely in different ways across distributions, require additional steps to make it work great for you on your PinePhone. It would be beyond the scope of this simple blog post to try to outline them all, but the ["Recommended Settings" document](https://github.com/Biktorgj/pinephone_modem_sdk/blob/honister/docs/SETTINGS.md) by Biktorgj is definitely a good start.

### Something wrong? Or: What about my distribution?

If you know that it works with your distribution, please go ahead and describe it after hitting that edit link below. I'll happily add it to this post after a brief review. If something is wrong with these instructions, I appreciate your feedback even more!

[^0]: Hint: You'll need to do a few things that I've explained in my [original post about installing this firmware](https://linmob.net/flashing-biktorgjs-modem-firmware/) to go back, e.g. it's adventageous to have `atinout`, `adb` and `fastboot` installed.

[^1]: Dylan has also worked on a lot of other critical improvements regarding the PinePhone and it's modem, e.g. it's his effort that got wakeup on incoming call from "well, this is sort of workable, maybe" to "this is actually quite good".

[^2]: I've tried to do this on [postmarketOS with Plasma Mobile and Sxmo as well, but failed](https://fosstodon.org/web/@linmob/107896837306369833).

[^3]: dd, Balena Etcher, GNOME Disks, ...

[^4]: If you want to be extra smart, write `gnome-firmware-updater` when pmbootstrap init asks for extra packages. Alternatively, read "Alternative: Use DanctNIX".

[^5]: Seriously, do it! On the PinePhone Pro do it, because power management is not nearly fully baked yet, and on the PinePhone do it or you just won't see that "Show releases" button.
