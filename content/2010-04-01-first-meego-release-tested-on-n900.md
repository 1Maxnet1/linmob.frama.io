+++
title = "First MeeGo release, tested on N900"
alieases = ["2010/04/01/first-meego-release-tested-on-n900"]
layout = "post"
author = "peter"
date = "2010-04-01T11:28:00Z"
[taxonomies]
tags = ["Intel", "Intel Atom", "MeeGo", "N900", "Nokia"]
categories = ["software", "shortform",]
+++
<a href="http://www.h-online.com/open/news/item/First-MeeGo-code-release-968944.html">Yesterday, not today (April Fools day) the first MeeGo release came out of the dust.</a>
MeeGo, which is basically a mash up of Nokias Maemo and Intels Moblin and was announced on Mobile World Congress 2010 will be a powerful platform for Atom and ARM devices&mdash;a strong opponent to Android, finally.

But it isn't ready yet, this is just an &#8220;opening of the codebase&#8221;&mdash;the first &#8220;real&#8221; release is expected in May. Take a look at this video of MeeGo on the N900.
