+++
title = "HP announce to kill webOS devices, look for licensees"
aliases = ["2011/08/hp-announces-to-kill-webos-devices.html"]
date = "2011-08-18T23:15:00Z"
[taxonomies]
categories = ["software", "commentary"]
tags = ["HP Pre3", "HP Touchpad", "HP Veer", "webOS",]
[extra]
author = "Peter"
+++
_Imagine you were spending a day at work lifting boxes and while doing so you would think of what kind of blog article you were going to write later that day. This article would be an announcement, that you were going to write about some kind of a product, say a HP Pre 3 in the future because you just made the decision to get this device as your next primary phone. Later that same day, right before sitting down to write that aforementioned article, you would check twitter and see rumors of the very company making that very product was going to discontinue that, and not only that but the whole range of devices using the same software platform._
<!-- more -->
_Well, this is what happened to me today. And you know what? I will buy a Pre3, anyway._

This was quite a long introduction to a rather sad story. HP has, about 16 month after purchasing Palm, decided that they will stop producing webOS devices later this year. While this doesn't neccessarily mean that webOS is all dead, this is sad news for me as a person that likes choice and loves Linux, and most importantly, loves webOS.

I haven't been happy with HPs progress with webOS anyway, but this news is a huge disappointment, especially because it always felt like that HP hadn't really started to push webOS forward: The Pre2 was a lot, but definitely not too exciting and little more than a ruggedized and sped up Pre (Plus), the Veer has this special form factor which doesn't make it too attractive for many (even though it has become really cheap recently here in Germany - you have to pay a little more than 150 EUR) and the Pre3 is just being launched. HPs TouchPad isn't a flawless product, but it's nice - initial pricing was way of and the fact that it was released in what one may call a pre beta stadium is a real disappointment. These are all the products HP has launched, 3 of them only very recently and yet they are pulling the plug. Knowing something about business I do understand that measures are possible this early, but seriously: They didn't launch one exciting smartphone and postponed the one, that could have been exciting for more than six months, they released a tablet which, in terms of look and feel, is unfortuntely inferior to the first iPad (Samsung somehow managed to quickly crank out an even thinner Galaxy Tab 10.1 in about the same time it took HP from announcing to selling the TouchPad) - it's all a sad story. And the software: I would have hoped for HP to move forward at a faster pace there, too.

I could start to critisize HP webOS even more, but I will do so, when I've got the Pre3 - if necessary. I like this software, and webOS is a weak platform right now, which doesn't need any harsh words - it needs (and deserve) soft care instead. Just saying: Even with the flawed Pre Plus hardware (which is already a lot better than the original Pre) I love to use webOS for its ease of use that you feel once you are used gestures and cards. HP, btw, announced a purchase and that they are actively considering to externalize their PC business - this day could be the day that marks the end of HP as we know it. I, for one, feel like Léo Apotheker is trying to make HP another SAP - I may be wrong there, but seriously, I believe most of you didn't see as HP as a software company until now.

The future is open, it will always be. I am excited to get my hands on the HP Pre3 and hope for some licensees that will crank out the kind of products webOS needs to become what it maybe could not become with hardware made by HP / Palm: A Success.

__SOURCES / READ MORE:__

* <a href="http://live.thisismynext.com/Event/HP_Q3_2011_earnings_call">thisismynext (the fellows that will become &#8220;The Verge&#8221;) have a great live blog of HPs Q3&#160;2011 earnings call</a>
* <a href="http://thisismynext.com/2011/08/18/hp-killed-webos-devices/">thisismynext: HP killed webOS devices</a>

