+++
title = "LinBits 15: Weekly Linux Phone news / media roundup (week 42)"
aliases = ["2020/10/18/linbits15-weekly-linux-phone-news-week42.html", "linbits15"]
author = "Peter"
date = "2020-10-18T10:43:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "PineTab", "PineCom", "Librem 5", "Purism", "PlasmaMobile", "Rockbox", "Arch Linux ARM", "Manjaro", "OpenSUSE", "LINMOBapps", "Fedora", "SailfishOS", "Unboxing", "BleedingTooth"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Distribution updates, #bleedingtooth, new PINE64 accessories, a new Sailfish release and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements 

* [DanctNIX Mobile/Huong Tram Linux/Arch Linux ARM by danct12 has seen a new release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20201015). _The biggest feature here is the new kernel, which even brings better phone call quality and might improve battery life because of Megi's modem driver. Please [run an upgrade after installing](https://fosstodon.org/@danct12/105040705058664430) to make sure you are save and the "#bleedingtooth" vulnerability can't be used against you._
* [Manjaro have released their first beta release for the PinePhone](https://forum.manjaro.org/t/manjaro-arm-beta1-with-phosh-pinephone/32307), which is featuring Phosh. _This indicates that their Community Edition is not shipping with Lomiri, as originally planned, but with Phosh, which arguably is more mature on the PinePhone. Even though this "beta release" may sound more mature than Arch Linux ARM above, a look at their [bug tracker](https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/phosh/-/issues) indicates that this is likely not the case._ 
* [OpenSUSE has seen a new release, too](https://nitter.net/hadrianweb/status/1316413354861359107). _I really need to try OpenSUSE._

### Worth Reading

* Phoronix: [Linux 5.9.1 + 6 Other Stable Kernels Out For Addressing "Bleeding Tooth" Vulnerability](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.9.1-Released). _This vulnerability affects PinePhone distributions, and so far only Arch Linux ARM (see above) [have announced they fixed it](https://nitter.net/DanctNIX/status/1316817237215383552#m). If unsure, do bother your distribution maintainers about this, so that it gets fixed on your favourite PinePhone distribution, too._
* xnux.eu log: [Backlight changes when switching between USB-C power mode](https://xnux.eu/log/#022). _Apparently, the Manjaro Community Edition is going to feature a small hardware change, which is going to fix a small bug again. And this might be another one of these that can be done at home._
* Adrian Plazas: [Specify Form-Factors in Your Librem 5 Apps](https://aplazas.pages.gitlab.gnome.org/blog/blog/2020/10/15/specify-form-factors-in-your-librem-5-apps.html). _While this is not the official FreeDesktop.org standard one would like to have, it is a good temporary fix for now._[^1]
* PINE64 Blog: [Update: new hacktober gear](https://www.pine64.org/2020/10/15/update-new-hacktober-gear/). _This update is very long and contains a lot, which is why I decided to link to derivative posts that break out parts of this massive announcement._
  * LinuxSmartphones.com: [Swappable PinePhone back covers will bring wireless charging, NFC, and a keyboard](https://linuxsmartphones.com/swappable-pinephone-back-covers-will-bring-wireless-charging-nfc-and-a-keyboard/) and TuxPhones.com: [⚡ Official PinePhone back cover will add hardware keyboard, wireless charging, NFC and more](https://tuxphones.com/pinephone-cover-qwerty-external-keyboard-wireless-qi-charging-nfc-5000mah-battery/). _This is cool. I have seen quite a few people in my time watching the keyword "PinePhone" on the birdsite that missed NFC, wireless charging or a hardware keyboard on the PinePhone. Unfortunately, there is no ETA yet. I would have preferred a slide out keyboard._
  * LinuxSmartphones.com: [Upgrade your PinePhone: 3GB/32GB mainboards coming in November](https://linuxsmartphones.com/upgrade-your-pinephone-3gb-32gb-mainboards-coming-in-november/). _This is great for everybody with buyers remorse who bought into the Braveheart or the UBports Community Edition._ 
* Jolla Blog: [Sailfish OS Pallas-Yllästunturi is now available](https://blog.jolla.com/sailfish-os-pallas-yllastunturi-is-now-available/). _Make sure to read [Brad's post on this](https://linuxsmartphones.com/sailfish-os-3-4-brings-multi-user-support-browser-improvements-and-more/), too. There are no 3.4.0.24 images released for the PinePhone yet, but I managed to upgrade thanks to [Adam Pigg's instructions](https://nitter.net/adampigg/status/1317511321840193537#m). That aside, this is a great release and I am glad that they are beginning to tackle ARM64 support &mdash; also I am amazed that the Jolla Phone, released in 2013, is still supported after seven years._
* Phoronix: [F2FS With Linux 5.10 Brings Many Improvements And A Few More Features](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.10-F2FS). _If you have A/B-tested [Mobian's](https://www.mobian-project.org/) F2FS image vs. the standard ext4 image, you will know which difference in speed and usability this filesystem can make. Great to see further improvements to this filesystem._
* Gamey: [Feed readers on the PinePhone](https://lbry.tv/@gamey:c/Feed-reader-for-the-Pinephone:7). _If you look at the post, you will surely realize why I just had to put this in._ 

### Worth Watching

* The Linux Experiment: [Software Ecosystems are bad, but Linux needs one](https://www.youtube.com/watch?v=q7tAGOjwx0M). _I do agree – sort of. I would rather see a web page that has good self-hosting tutorials and shows a nice collection of local companies offering services based on open source software for those that don't feel up to self-hosting infrastructure._
* PizzaLovingNerd: [PineTab Unboxing and First Impressions](https://www.youtube.com/watch?v=v2tw7BGdzsg). _Nice first PineTab impressions by Pizza here._
* PizzaLovingNerd: [FOSS Spot: Plasma 5.20, Plasma Mobile, Fund Your App, Kernel 5.9, Enso OS, Pitivi](https://www.youtube.com/watch?v=fiyfHHZkIx8). _Not PinePhone centric, but it is a nice collection of news and I share the opinions on PlasmaMobile and "Fund Your App"._
* Дмитрий Власенко: [Возвращение PinePhone или ложка меда в бочке кода](https://www.youtube.com/channel/UCLm0nczjyPvEjqC1xVIg3RQ). _This is a russian video. I did not understand a thing, but if you do, it should be interesting._
* Sakari Castrén: [Pinephone; cellular, wifi etc working](https://www.youtube.com/watch?v=PRtHcdWWowc). _A video showing postmarketOS onboarding._
* Avisando: [PinePhone - SD Card - USB C - Jack](https://www.youtube.com/watch?v=HQgqfGtWrDU). _This is a short video that may help new PinePhone users with onboarding._
* Alex_Davis: [(RaaA) Rockbox as an application on Pinephone](https://lbry.tv/@Alex_Davis:8/(RaaA)-Rockbox-as-an-application-on-Pinephone:b). _You may remember Rockbox as an alternative software for MP3 players, but you can run it on the PinePhone as an application, too. Unfortunately, the video seems to have no sound, so we can only guess whether audio playback works._
* DanctNIX: [Arch Linux ARM booting on Redmi Note with Phosh UI](https://nitter.net/DanctNIX/status/1317614986651267072#m). _This is using Halium and not a mainline kernel, but it is still impressive and nice to see._

### Stuff I did

Not much:
* Most notably, [I was able to merge the first addition to LINMOBapps](https://framagit.org/linmobapps/linmobapps.frama.io/-/commit/f7a8a72baeb644be1ae5127c6350d02d3caed223) that reached me via E-Mail. If there is any app you tried on your PinePhone which somewhat worked and is not on the list yet, _please mail in and help me get this list up to the 200 apps mark!_
* I played around with SailfishOS: I upgraded my PinePhone to 3.4.0.24, [installed Sailfish X on a SONY Xperia X](https://nitter.net/linmobblog/status/1317482767811301376#m) to have a point of comparison and will finish my Sailfish video right after publishing this.
* I installed UBports on a new device using the excellent UBports installer. I might mention it as a point of comparisonm excited to see those projects progress in the mobile space and hope you enjoyed todays post. As a end note check out LinMob he runs a amazing feed in case you want to stay in the loop on the Pinephone and mobile Linux in general. I also made extensive use of his mobile Linux App list which you can find here to make this blog post in  in a future video on UBports' progress on the PinePhone (which is not going to happen before November). 
* I [updated](https://nitter.net/linmobblog/status/1316403709249818625#m) the [MaemoLeste install](https://leste.maemo.org) on my Motorola Droid 4, and while they still lack apps for communication ([it is going to happen though](https://nitter.net/linmobblog/status/1316779362067329024#m)), screen rotation and a calendar app have been added, and the OS is running a lot smoother overall. 

[^1]: I forgot this to include this story initially. It has been added at 21:30 CEST on October 18th, 2020.
