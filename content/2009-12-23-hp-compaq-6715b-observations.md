+++
title = "HP Compaq 6715b: Observations"
aliases = ["2009/12/23/hp-compaq-6715b-observations.html"]
author = "peter"
date = "2009-12-23T17:15:00Z"
layout = "post"
[taxonomies]
tags = ["HP 6715b", "x86", "notebooks"]
categories = ["personal", "impressions"]
+++
The fact that I am writing this on my good old HP nx6325, which is the 6715b's direct predecessor shows that I am not yet completely sure whether I will keep the 6715b&mdash;though it is a improvement in many ways (though more modern units are supposed to be better). 

One example is the screen, which is marvelous. After a few days of using the 6715b, when I went back to its predecessor, I was able to count pixels&mdash;XGA on 15" is not exactly what you would call high resolutions. Besides this, brightness and colours are a lot better on the 15,4" WSXGA+ screen of the 6715b&mdash;just as expected. 

Linux hardware support is ok as well, with Xubuntu 9.10 (x64) everything works out of the box (ok, you have to install extra software for bluetooth and the fingerprint reader (I didn't use the latter yet, as I love passwords ;) ) and WiFi firmware). But as you might have guessed, there are some downsides of the &#8220;new&#8221; unit. One is heat and loudness.

Yes, it is loud, and this is not a linux issue, it is the same with Windows Vista (which is a ressource hog). And: though the fan is ok (not suffering from too much dust or sth like that) overheating is something that happens&mdash;you don't have to be a genious to imagine that I don't like that. Apparently the main reason for this is the ATI X1250 integrated graphics, which is said to become pretty hot&mdash;I will have to investigate in that issue on the software side (unfortunately there are no proprietary ATI drivers for current kernels), will try whether some quality heat paste helps&mdash;if it does, I might keep the 6715b.

But even then I would still suffer from rather bad southbridge performance&mdash;the AMD SB600 chipset seems to be not that well supported as the SB400 is, e.g. I cannot use my G1 as a USB 3G modem and watch DVB-T at the same time&mdash;it works with the nx6325.

Overall I am not that satisfied. Which is rather sad, but that's what life is like sometimes.
