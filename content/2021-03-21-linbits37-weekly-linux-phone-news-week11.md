+++
title = "LinBits 37: Weekly Linux Phone news / media roundup (week 11)"
aliases = ["2021/03/21/linbits37-weekly-linux-phone-news-week11.html", "linbits37"]
author = "Peter"
date = "2021-03-21T21:45:00Z"
layout = "post"
[taxonomies]
tags = ["PINE64", "PinePhone", "Purism", "Librem 5", "LINMOBapps", "PINE64 Community Update", "Phosh", "Plasma Mobile", "Maui", "Manjaro", "postmarketOS", "Mobian", "Pangolin Mobile"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New Manjaro betas, phoc 0.7.0, Megi's log is back and more! _Commentary in italics._
<!-- more -->

### Software development and releases

* phoc 0.7.0 has been [released](https://social.librem.one/@dos/105917684552693832), [full release notes](https://source.puri.sm/Librem5/phoc/-/releases/v0.7.0). _It's more stable and has some convergence improvements._
* Manjaro have released two new betas:
    * [Phosh beta 7](https://github.com/manjaro-pinephone/phosh/releases/tag/beta7),
    * [Plasma Mobile beta 3](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta3). _Both deliver the latest software._
* Also based on Manjaro now, Nemo Mobile have finally put out a [new image](http://img.nemomobile.net/2021.05/) for the PinePhone. _It still needs work._

### Worth noting
* Purism forums: [VoIP/SIP from the L5](https://forums.puri.sm/t/voip-sip-from-the-l5/12651). _Make sure to read that first comment, and don't tell anyone._

### Worth reading 
* PINE64: [March Update: Status Report](https://www.pine64.org/2021/03/15/march-update/). _Another great update. Let's hope that the component shortage won't get worse._
* PINE64: [Beta Edition Pre-Orders](https://www.pine64.org/2021/03/19/beta-edition-pre-orders/). _Nice! It will have a different compass/magnetometer, but otherwise it's similar hardware to the last three Community Editions and a more developed variant of the KDE CE's software._
  * Linux Smartphones: [PinePhone Beta Edition goes up for pre-order March 24 (updated)](https://linuxsmartphones.com/pinephone-beta-edition-goes-up-for-pre-order-soon/). _Brad's take._
* xnux.eu: [Some PinePhone updates](https://xnux.eu/log/#031). _Megi reports on his progress and plans for the first time in 2021. Make sure to read this._
* xnux.eu: [PinePhone eMMC measurements](https://xnux.eu/log/#032). _Some benchmarks. BTW: The Mobian Wiki has [more of this](https://wiki.mobian-project.org/doku.php?id=benchmarks)._
* Linux Smartphones: [Ubuntu Touch OTA-16 released with improved browser, video recording, Android app support and more](https://linuxsmartphones.com/ubuntu-touch-ota-16-released-with-improved-browser-video-recording-android-app-support-and-more/). _This is a great release. Remember: The PinePhone, using a mainline kernel and Wayland instead of the Mir protocol, does not follow the OTA schedule. Don't be sad though, there's been progress on the 'kernelupgrade' channel which makes me quite hopeful._
* Linux Smartphones: [ Pangolin Mobile is a new open source user interface for smartphones](https://linuxsmartphones.com/pangolin-mobile-is-a-new-open-source-user-interface-for-smartphones/). _This looks like it could develop into something cool._
* TuxPhones: [The JingPad A1 is a Linux tablet with 6GB RAM, 2K display, 5G and much more](https://tuxphones.com/jingpad-a1-linux-tablet-5g-2k-6gb-ram-jingos-preorders/). _PowerVR graphics. Meh. Great article though!_
* OSNews: [The Nokia N900: the future that wasn’t](https://www.osnews.com/story/133160/the-nokia-n900-the-future-that-wasnt/). _A nice history lesson._
* Disputatio Ebrius: [HOWTO Create A Working Pine Phone Image From The eMMC Using dd](https://mstdn.design/howto-create-a-working-pine-phone-image-from-the-emmc-using-dd). _Nice article!_
* Dylan Kirdahy: [How to use your Protonmail account with Geary on your Pinephone (Mobian, in this case)](https://dylankirdahy.nyc/2021/03/12/how-to-use-your-protonmail-account-with-geary-on-your-pinephone-mobian-in-this-case/). _If you use non-standard-following email providers, this is surely helpful._
* Kevin's musings: [Pinephone and Fedora](https://www.scrye.com/wordpress/nirik/2021/03/20/pinephone-and-fedora/). _Interesting read, altough I would advise differently in many cases, especially regarding scaling and Matrix clients._
* Mobian: [2021-03-17 Status Update](https://blog.mobian-project.org/posts/2021/03/17/update-2021-03-17/). _Boring title, interesting read._
* Mobian: [The unstable Mobian distro](https://blog.mobian-project.org/posts/2021/03/15/unstable-distro/). If you want Mobian and more recent software, you now have a choice.
* NitruxOS: [Maui Weekly Report 9](https://nxos.org/maui/maui-weekly-report-9/). _Another great progress update on Maui!_



### Worth listening
* rav3ndust: [Why Are Linux Phones Important? - FOSScast #1](https://www.youtube.com/watch?v=9vC46Bfd2xo).

### Worth watching
* PINE64: [March Update: Status Report](https://tilvids.com/videos/watch/7dc55ca2-c27e-47fa-9eb7-68f1047769c9). _Great video summary by PizzaLovingNerd!_
* Short Circuit: [A $200 phone that can do ANYTHING!!! - Pine64 Pinephone](https://www.youtube.com/watch?v=fCKMxzz9cjs). _After having had a look at the Librem 5, Anthony now tries the PinePhone. Let's just say that they could have done more research._
* AnotherKiwiGuy: [The PinePhone Review - March, 2021](https://media.kaitaia.life/videos/watch/f072045c-b713-48c6-9ce5-8d2d8f7d7e76). _While this video will likely never have as many views as the previous one has now, it does a way better job in my opinion. Great!_
* Camden Bruce: [Pangolin Mobile running on the pinephone](https://www.youtube.com/watch?v=D-_Pzb9wlXQ). _A promising new UI written in Flutter._
* Linux Lounge: [Mining Cryptocurrency On The PinePhone and PineTab](https://odysee.com/@LinuxLounge:b/mining-cryptocurrency-on-the-pinephone:4). _Just in case you're bored and need something to do uh mine._
* Jolla Devices: [Magsafe card holder for Pinephone // Pinephone is magnet ready!](https://www.youtube.com/watch?v=LMTpoSnwfbo). _It's fun!_
* Camilo Higuita: [Maui Project - Progress Update](https://www.youtube.com/watch?v=YNXPr5muS6s). 
* Polymath Programming: [Raylib on Mobian PinePhone](https://www.youtube.com/watch?v=Vi4TXnrnExo). _Nice!_
* Privacy & Tech Tips: [Part I: Linux/Pinephone SSH Securing + Hydra Demo On Default Pins (Plus Fixes)](https://www.youtube.com/watch?v=GAvGKlvKouY). 
* Martijn Braam: [Linux 5.11 on Motorola Osprey with postmarketOS](https://www.youtube.com/watch?v=RrNILC1WZmY). _For more information on the Osprey/Motorola Moto G 2015, [click here](https://wiki.postmarketos.org/wiki/Motorola_Moto_G_2015_(motorola-osprey))._

#### Librem 5 convergence corner
* Auberge du Tilleul: [Librem 5 Convergence vidéo VLC part 1](https://www.youtube.com/watch?v=vQ9xP3WWrMo), [2](https://www.youtube.com/watch?v=KiGpyqvkJ7k), [3](https://www.youtube.com/watch?v=QDbV215ii9k), [4](https://www.youtube.com/watch?v=RUsG8L3j3w0), [5](https://www.youtube.com/watch?v=xNp8_q3N_lc).

### Stuff I did

#### Content

I made a video, it's called "Mobian on the Purism Librem 5 Evergreen" and you can watch it on
* [DevTube](https://devtube.dev-wiki.de/videos/watch/2addeee5-0352-4c7b-b7ed-f44a140b9dfb), or
* [Odysee](https://odysee.com/@linmob:3/mobian-on-the-purism-librem-5-evergreen:f),
* [YouTube](https://www.youtube.com/watch?v=7_4zCOTELlU).

#### LINMOBapps

The [gaming list is maintained again](https://fosstodon.org/@linmob/105899448551031619)! Thanks to moxvallix for adopting it! Sadly, I did not make any progress on working through tasks.md. [See here what happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). Please [do contribute](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/tasks.md)! 

 

