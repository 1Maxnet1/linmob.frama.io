+++
title = "Back to QWERTY"
aliases = ["2015/02/28/zurueck-zur-tastatur.html"]
author = "peter"
date = "2015-02-28T13:51:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "Android 5.0 (Lollipop)", "needs translation", "english", "hardware keyboard", "LG G2", "linmob", "qwerty", "smartphones", "Droid 4",]
categories = ["personal", "commentary",]
+++
<a href="http://brimborium.net/wp-content/uploads/2015/02/2015-02-28-14.33.40.jpg"><img src="http://brimborium.net/wp-content/uploads/2015/02/2015-02-28-14.33.40.jpg" alt="Motorola Droid 4 und LG G2" class="size-full wp-image-3344" /></a> Droid 4 trumps the LG G2

In November 2013, I bought a new smartphone for the last time so far. It was an LG G2. Since then, a few new devices have been released (yes, now it's Mobile World Congress again soon, the device flood has already experienced the first advance "leaks"), but even though I had constantly spent too much money on all kinds of smartphones over the years before, I soon had to realise that the quick change suddenly didn't make sense anymore. Yes, the LG G3 is a bit better, but somehow not really (who needs this display resolution, and why is the thing even bigger?). I don't touch Samsung for reasons (TouchWiz, hardware design), HTC has 4 megapixel cameras and the cases are too big compared to the display.
<!--more-->
But there was a drawback. The text input. Admittedly, with a 5.2" display diagonal in 16:9 format, even I can type to some extent with my sausage fingers. But: 
1. I can't type blindly, 
2. at least for the German language, the standard dictionaries of various smartphone keyboards are not quite adequate for my vocabulary, 
3. what do autocorrect allow?

I then went back to smartphones with hardware keyboards and returned to the Motorola/Verizon Droid 4, which I had already tried out in 2012 in semi-defective condition. Honestly, it wasn't a conscious decision. I bid $ 42 USD on a Droid 4 on eBay just for fun. With shipping from the US including import VAT, it cost about € 50 at the exchange rate, which was different at the time. I received a functional, albeit relatively scuffed device on which, unfortunately, the earpiece was overloaded at one point and the headphone jack doesn't work either. Otherwise it's fine. And above all, Android 5.0 "Lollipop" in the form of Cyanogen Mod 12 is available for this device from early 2012, which, incidentally, is the last high-end smartphone with a hardware keyboard and SIM card slot to date.

I have since acquired another Droid 4 in better condition for €75, which does not have the shortcomings of the other device. Meanwhile, the LG G2 is gathering dust. Yes, the Droid 4's display is not great. Yes, the browser is slowed down by the hardware here. Yes, the camera of the LG G2 is clearly better. But at last I can write my thoughts in a note app on the go without having to constantly check with algorithms that this long word is actually in the dictionary or that my neologisms shouldn't be destroyed. As a result, I can write down my thoughts undisturbed, no longer write rubbish in chat apps, and writing on the go is fun again. 

__TLDR:__ Hardware keyboard = improved quality of life.
