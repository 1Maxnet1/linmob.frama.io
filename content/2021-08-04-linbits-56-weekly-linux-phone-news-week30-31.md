+++
title = "LinBits 56: Weekly Linux Phone news / media roundup (week 30/31)"
date = "2021-08-04T21:56:00Z"
updated = "2021-08-05T17:05:00Z"
[taxonomies]
tags = ["PinePhone", "Librem 5 USA", "LINMOBapps", "LinuxPhoneApps",]
categories = ["weekly update"]
[extra]
author = "Peter"
update_note = "Removed link to a Reddit post that was removed by its author, later replaced it with a link to the orginal forum thread this Reddit post was linking."
+++

_It's Wednesday. Now what happened since last Wednesday?_

Sxmo, Chatty, Megapixels and others see new releases, Phosh & co move to GNOME GitLab, and more!<!-- more --> _Commentary in italics._ 


### Software Releases
* [Megapixels 1.2.0 has been released](https://git.sr.ht/~martijnbraam/megapixels/refs/1.2.0). _Autofocus is now no longer blocking!_
* [Chatty 0.3.4 is out, bringing a number of bug fixes and enhancements](https://source.puri.sm/Librem5/chatty/-/commit/7cbabfb1f6a1b0f63c683cd6430fe95bb313c199). 
* [Sxmo 1.5.0 has been released](https://lists.sr.ht/~mil/sxmo-announce/%3C20210801111149.kr3nlf3g4v6ybvml%40worker.anaproy.lxd%3E). _Make sure to read Brad's post about it!_
* [OpenSUSE Tumbleweed for the PinePhone has also made some progress](https://twitter.com/hadrianweb/status/1422187931620368392)!

### Worth noting
* Phosh has moved to [GNOME's GitLab](https://gitlab.gnome.org/World/Phosh/phosh), so have [Squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard) and [phoc](https://gitlab.gnome.org/World/Phosh/phoc).
* [Notifications are coming to Phosh's lockscreen](https://social.librem.one/@agx/106670797835144343)!
* @powen00hsiao managed to get [checkra1n running on PinePhone](https://twitter.com/powen00hsiao/status/1421803816421257227/video/1).
* There's a [thread on the Purism forums](https://forums.puri.sm/t/librem-5-usa-read-before-ordering/14239) that you actually may want to read before ordering.

### Worth Reading

#### Expensive Unboxings
* Amos B. Batto: [Unboxing the Librem 5 USA](https://amosbbatto.wordpress.com/2021/07/30/unboxing-the-librem-5-usa/). _A good, long read!_

#### Alternatives to the Duopoly
* The Reboot: [Cracking Open the Android/iOS Grip on Smartphones and the Mobile Internet](https://thereboot.com/cracking-open-the-android-ios-grip-on-smartphones-and-the-mobile-internet/). _Rather boring._

#### Software Progress
* Claudio Cambra: [Meeting with your mates — Kalendar week 8 (GSoC 2021)](https://claudiocambra.com/2021/08/01/meeting-with-your-mates-kalendar-week-8-gsoc-2021/).
* Volker Krause: [June/July in KDE Itinerary](https://www.volkerkrause.eu/2021/08/01/kde-itinerary-june-july-2021.html). _Great progress, health certificate support really matters these days!_
* Martín Abente Lahaye: [Portfolio 0.9.11](https://blogs.gnome.org/tchx84/2021/07/31/portfolio-0-9-11/). _Another update to my favourite mobile file management application!_
* Philippe Normand: [Introducing the GNOME Web Canary flavor](https://base-art.net/Articles/introducing-the-gnome-web-canary-flavor/). _Not too mobile specific, but given the fact that I recently made GNOME Web my default web browser on Phosh, I am adding this so I don't forget to try it out eventually._
* manugen: [A quick update on libadwaita’s animation API](https://blogs.gnome.org/manugen/2021/08/04/a-quick-update-on-libadawaitas-animation-api/).
* NxOS: [Maui Report 14](https://nxos.org/maui/maui-report-14/). _The 2.0 releases of the convergent Maui apps are going to be awesome!_
* Linux Smartphones: [Sxmo 1.5.0 released with networking, screen lock, and UI improvements](https://linuxsmartphones.com/sxmo-1-5-0-released-with-networking-screen-lock-and-ui-improvements/). _Brad wrote down the hightlights of the new Sxmo release. Well done!_

#### Documentation
* halting problem: [Documenting GNOME for developers](https://www.bassi.io/articles/2021/08/01/documenting-gnome-for-developers/). _I really like that new website!_

### Worth Listening
* postmarketOS Cast: [#7 v21.06, A-GPS, Megapixels, Bluetooth, gPodder, iPhone 7](https://cast.postmarketos.org/episode/07-AGPS-Megapixels-Bluetooth-iPhone7-gPodder/). _Another great episode! And thanks for the kind words about PineTalk!_

### Worth Watching

#### Software progress
* Martijn Braam: [postmarketOS tweaks and Megapixels update](https://www.youtube.com/watch?v=3DoxSQ_jZ0w). _Nice updates!_
* Jozef Mlich: [Nemomobile on PinePhone July 2021](https://www.youtube.com/watch?v=IV1uIGQxENY). _Nice video about what happens in Nemo Mobile. Also I must say that I am totally envious of that Nokia N950 lying there._

#### Ubuntu Touch corner
* UBports: [Ubuntu Touch Q&A 105](https://www.youtube.com/watch?v=9hwHwvbVhhc). _This time, Dalton and Alfred discuss the changes of the past two weeks together, including, but not limited to: the updated Porting documentation, Waydroid and what it means, 5.13 kernel merged for PinePhone and PineTab, impressive Focal news. They ask for help with Pull Requests, as there are a 170 open PRs needing review._
* Digital Wandering: [6 Reasons to Use Ubuntu Touch](https://www.youtube.com/watch?v=hIY8w8qK_60). 


#### Usability  reviews
* Arbitrary Tech: [Pinephone Usability Review 2021](https://www.youtube.com/watch?v=ZDUUm9Zr6Zk)

#### Software comparisons
* PizzaLovingNerd: [Phosh vs Plasma Mobile (Side by Side Comparison)](https://odysee.com/@pizzalovingnerd:5/phosh-vs-plasma-mobile-\(side-by-side:8?). _I always wanted to do a similar video, but given the differences between my PinePhones, I never did._

#### Tutorials
* kdlk.de: tech: [PinePhone Development: How to Cross Compile to ARM - Setup #gtk #rust #AppDevelopment #Arch](https://www.youtube.com/watch?v=9jlweZLwWZA). _All these difficulties make more happy with my bad habit of compiling on device._
* kdlk.de: tech: [PinePhone Essentials: #Phosh Screenshots on the PinePhone #grim #scrot](https://www.youtube.com/watch?v=rcibP3X9B_o).

#### Unboxing
* Amos Batto: [Unboxing the Librem 5 USA](https://www.youtube.com/watch?v=OwcJnNrbxJA). _Make sure to read the video's description, it's quite verbose._

#### Getting started and experimenting
* Jacob David Cunningham: [My Pinephone experience](https://www.youtube.com/watch?v=nx2UpCVmJ9o). _Technically it's not an unboxing, but it starts second after that, continuing much farther though._

#### JingOS

* TechHut: [This Linux tablet runs Android Apps!... kinda](https://www.youtube.com/watch?v=XDxr8B8xibQ). _Android apps on JingOS!_

### Stuff I did

#### Content

I finally made another a video this week. It's called "PinePhone Software Progress: Phosh's App Drawer, Video Acceleration, Kasts and more". Go watch it on [DevTube](https://devtube.dev-wiki.de/videos/watch/095bc753-7cf3-42cf-94a6-8ee7c289377e), [Odysee](https://odysee.com/@linmob:3/pinephone-software-progress-phosh's-app:3) or [YouTube](https://www.youtube.com/watch?v=C4RC3Miuo2A).

#### Random

Before writing this, I made some progress with regard to getting the prototype of LinuxPhoneApps.org ready for takeoff. I hope to get it online before the next update.

#### LINMOBapps

I have started to review app entries from the top onwards. Also, these apps were added in the past week, upping the App count to 306:

* [foKus](https://invent.kde.org/rowdyninja/fokus), a To-do application, designed for Plasma Mobile using Kirigami framework,
* [tvtoday](https://github.com/Schmiddiii/tvtoday), an application to inspect the television program of today, made for the PinePhone,
* [LaTre](https://github.com/hongquan/LaTre), an assistant tool for GNOME Contacts, that does import contacts from VCard files,
* [modRana](https://github.com/M4rtinK/modrana), a flexible GPS navigation system for mobile devices. _Great app, it really should be packaged in more distributions. To help with this, [I created a PKGBUILD script that builds the current state from git](https://framagit.org/linmobapps/pkgbuilds/-/tree/main/modrana-git)._
* [Satellite](https://codeberg.org/tpikonen/satellite), which displays navigation satellite (GPS) information from ModemManager enabled devices (thanks to _tpikonen_ for creating and adding this one!),
* [Kalendar](https://invent.kde.org/pim/kalendar,,https://claudiocambra.com/category/kde/), a WIP calendar application using Akonadi to sync with external services (NextCloud, GMail, ...).

The easy [backlog](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/4) is now almost done, but I have also added more apps that need to be tested and then added..

[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master). 
Please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)
