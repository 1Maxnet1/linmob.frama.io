+++
title = "Widescreen laptops are dumb"
aliases = ["2018/04/24/widescreen-laptops-are-dumb"]
author = "peter"
comments = true
date = "2018-04-24T16:35:00Z"
layout = "post"
[taxonomies]
tags = ["16:9", "aspect ratio", "displays", "high resolution screens", "Laptops", "link"]
categories = ["shortform"]
+++
<a href="https://www.theverge.com/circuitbreaker/2018/4/19/17027286/laptop-widescreen-aspect-ratio">Vlad Savov, writing for The Verge on laptops and aspect ratio:</a>

<blockquote>"But a laptop is more than just a video playback machine. For myself and millions of others, it’s the primary tool for earning a living. We use these machines to read, write, remember, create, connect, and communicate. And in most of these other applications, a 16:9 screen of 13 to 15 inches in size just feels like a poor fit."</blockquote>

As a guy who among other laptops uses an ancient IBM ThinkPad X60s from time to time and does not own a single 16:9 laptop despite market realities, I obviously wholeheartedly agree.
