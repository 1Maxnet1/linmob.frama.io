+++
title = "Time as a problem"
aliases = ["2008/04/time-as-a-problem.html"]
date = "2008-04-27T17:42:00Z"
[taxonomies]
categories = ["software", "projects"]
tags = ["2.6.25", "A910", "angstrom", "debian lenny", "HTC Universal", "modding"]
[extra]
author = "Peter"
+++
My HTC Universal runs with Angstrom again, after a short look at <a href="http://wiki.neilandtheresa.co.uk/Titchy_Mobile">Debian (Titchy Mobile)</a>, which I'll use again as soon as there is a working X-Server&mdash;because htcunid is a great daemon; phone calls and even GPRS connection are no problem&mdash;I tried to integrate it into my Angstrom minimalist GPE image (autobuild unstable), but wasn't successful yet: I didn't notice phone calls, the caller could not hear me and SMS receiving did not work as on debian as well.
<!-- more -->
So if there will be a working X-Server in Lenny (armel) I'll try to use GPE on top of that, maybe I'll be able to expand my programming knowledge with a nice zenity or pyGTK frontend for htcunid.

And I would be very happy about further kernel development for HTC Universal, if there was someone who could create a working 2.6.25 image&mdash;it would be gorgeous&mdash;I heard that there is a camera driver available somewhere and that there have been some improvements on PXA support in mainline. I don't have the time and the knowledge to it (if I had the knowledge, it would take me less time and I could do it, I guess).

It is university season again, which means that I have to fill my brain with other things, economics, finance, e-business&mdash;and not with sweet little Linux gadgets.

That's the reason for the headline, I think I won't be able to go on with weeklies for A910&mdash;as I didn't get any response on my last weekly yet, I feel there is no need for weekly releases&mdash;I will work on it when I have the time do that and when I want to do it.
I've written a list of things to do, and as far as I am finished with it, I will just release it.

May take some months.
