+++
title = "One more phone for GNU/Linux"
aliases = ["/2018/11/28/one-more-phone-for-gnulinux"]
author = "peter"
date = "2018-11-29T18:27:00Z"
layout = "post"
[taxonomies]
tags = ["freescale", "GNU/Linux", "i.MX6", "KDE", "Librem 5", "links", "nxp", "Pine64", "PinePhone", "Plasma Mobile", "Purism"]
categories = ["shortform"]

+++
<a href="https://dot.kde.org/2018/11/29/necuno-mobile-open-phone-plasma-mobile">KDE</a> and <a href="https://necunos.com/mobile/">Necuno Solutions</a> today announced a new open source GNU/Linux smartphone that is supposed to be running a Plasma Mobile based interface. Based on the FreeScale/NXP i.MX6 SoC, which is quite dated, this thing certainly reminds me (in terms of the hardware) of what Purism initially intended to deliver. All in all, while I am interested, I will just patiently wait if this project will ever deliver anything: The software isn't there, the hardware does not seem to be totally defined yet. _Smells like vapourware to me._

(Actually, I find the <a href="https://itsfoss.com/pinebook-kde-smartphone/">PinePhone</a> more interesting, although the same concerns plus additional ones due to the SoC apply there, too.)

__VIA:__ <a href="https://liliputing.com/2018/11/necuno-mobile-an-open-source-smartphone-with-kde-plasma-mobile.html">liliputing</a>
