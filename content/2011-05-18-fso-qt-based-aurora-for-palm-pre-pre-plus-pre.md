+++
title = "FSO: Qt based Aurora for Palm Pre, Pre Plus, Pre²"
aliases = ["2011/05/18/fso-qt-based-for-palm-pre-pre-plus-pre.html", "2011/05/18/fso-qt-based-for-palm-pre-pre-plus-pre2.html"]
author = "peter"
date = "2011-05-18T10:12:00Z"
layout = "post"
[taxonomies]
tags = ["announcement", "aurora", "development", "FSO", "mobile devices", "palm pre"]
categories = ["software"]
+++

The world of mobile linux is like that today: There's a huge buzz about Android, less, but still some about HP webOS and MeeGo and very little about LiMo. But what about all those projects that started with Openmoko back in the days? Well, some of them are still alive and as that's why I share this announcement by mickey and morphis, which was posted to the <a href="http://www.shr-project.org/">SHR</a> mailing list a two days ago:

<blockquote>
Dear FOSS-Telephony lovers,

today we want to announce something that has been brewing in our minds for quite a while and will change the way we develop the <a href="http://freesmartphone.org/"  target="_blank">freesmartphone.org</a> middleware. 
In the past, FSO has been too much developed without considering how the features will actually be used by the API consumers. Apart from the great work our friends from SHR did, there has only been a handful of special purpose FSO clients, such as the Emacs client, Zhone, and Zhone2. Zhone (and its successor Zhone2) is currently an oversimplified approach based on a non-maintainable Edje file. We have therefore decided to develop a new testing/demonstrator for FSO named Aurora that is supposed to be the driving force for further development. 

### AURORA
The aim of Aurora is to replace zhone and zhone2 as development UIs for FSO. From the viewpoint of a middleware architect, it&#8217;s essential to have clients available that use the various features of the FSO services. On the other hand though, this time we want to create something that is also suitable for day to day use. Aurora is supposed to be something we call a &#8220;featurephone client&#8221;&#160;? featurephones being those things we used for telephony before smartphones were invented. 

Aurora being a featurephone client does not necessarily mean it will never get the &#8220;smartphone features&#8221; Android or iOS are popular for, it rather describes our approach as being as-simple-as-possible. So for now you will not be able to install additional apps or features. Everything (you need) is part of the Aurora client. 

### DEVELOPMENT PROCESS
At the top of every application stack is the user. Pleasing him or her is the topmost priority. Technology should not stand in the  way, but rather support the user. Hence, Aurora releases will be done as user milestones. For every user milestone, we will pick a number of user stories to be implemented. We will then split a user story into tasks and distribute among the contributors. 

### SUPPORTED DEVICES
We decided to only support the Palm Pre devices (Pre/Pre Plus/Pre 2) for the first to-be-released version of Aurora. More supported devices will join after the 0.1 release. This decision has been forced by the fact that we are only very few people working both on FSO and Aurora (and also on OpenEmbedded). Later on, we expect to see the OpenEZX family of devices, the Openmoko devices, the Nokia N900, and possibly also a bunch of HTC smartphones supported. 

### TECHNOLOGY
Some words about the technology choices we have made for Aurora. The UI components of Aurora will be based on Qt&#8217;s QML (Qt Markup Language) and will have parts written in C++ and Vala (although we&#8217;re going to use Python for prototyping as well). We will support both Qt/X11 and Qt/Embedded, the latter being useful on smaller systems, such as the OpenEZX family of devices (48MB RAM, no GFX acceleration, etc.) For the first release we will only provide Qt/Embedded based images for the Palm Pre devices; those flashable images will be based on OpenEmbedded, however we&#8217;d welcome people taking care of creating releases based on Debian, Gentoo, etc. 

### THE CODE
At the moment, there is not much to look at, but feel free to download the current status via <a href="http://git.freesmartphone.org/"  target="_blank">git.freesmartphone.org</a>   -&gt; aurora. 

### HOW TO HELP
Speaking about welcoming people, the major aim of this announcement is to find people who want to share this vision and give us a bit of a hand. We are especially lacking artists and folks who can improve our user interaction. Apart from the technical reasons, we chose QML to have a very low barrier of entry. QML is easy to understand and it also comes with a GUI design tool. >

If you are interested and share our vision, please feel free to contact us. We can then see how you could help us to get to the end goal (see roadmap) even faster. 

There are two possibilities to make us aware of you: 
- IRC:   <a href="http://irc.freenode.net/"  target="_blank">irc.freenode.net</a> ; channel: #openmoko-cdevel
- Mail:   
Thanks for your attention, 
Mickey &amp; Morphis.</blockquote>

Some may ask: Why should I use my smartphone as a featurephone? Well, you don't have to. But if you are interested in free software, this is a way to free your phone. Just think of it as a cool idea: My phone runs on free software. Pretty cool, huh?
