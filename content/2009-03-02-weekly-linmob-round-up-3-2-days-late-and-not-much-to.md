+++
title = "Weekly linmob round up (3): 2 days late and not much to tell, anyway"
aliases = ["2009/03/02/weekly-linmob-round-up-3-2-days-late-and-not-much-to-tell.html", "2009/03/02/weekly-linmob-round-up-3-2-days-late-and-not-much-to.html"]
author = "peter"
date = "2009-03-02 10:32:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "fennec", "FSO", "gta02", "GTA03", "news", "T-Mobile G1", "webos"]
categories = ["weekly update"]
+++

The last week was&mdash;assuming that I didn't miss all the interesting hot news&mdash;a rather silent week for those interested in Linux on mobile (meaning very mobile) devices. This wasn't surprising, anyway, as it was the week between MWC and CeBIT, so much noise was pretty unlikely&mdash;but that it had to be so silent&#8230; the silence wouldn't have been that bad, if it hadn't been so boring.

Have a list of links:
* <a href="http://www.engadgetmobile.com/2009/02/22/sim-technologys-u1-runs-android-at-vga-resolution-sort-of/">SIM Technology's U1 runs Android at VGA resolution [engadget mobile]</a>
* <a href="http://www.gizmodo.com.au/2009/02/hands_on_with_the_kogan_agora_android_phone_prototype.html">Hands On With The Kogan Agora Android Phone Prototype [Gizmodo Australia]</a>
* <a href="http://www.androidfanatic.com/cms/community-forums.html?%20func=view&amp;catid=9&amp;id=1615">Gnome, KDE, IceWM or LXDE Desktop on your Android! [android fanatic forums]</a>
* <a href="http://www.macworld.com/article/139045/2009/02/google_g1.html">Google blocks paid apps for Android Dev Phone 1 users [macworld.com]</a>
* <a href="http://www.wmexperts.com/fennec-will-sync-tabs-now-just-firefox">Fennec will sync tabs, now just called Firefox [WM Experts]</a>
* <a href="http://www.youtube.com/watch?v=YXS3SQauwPE">O'Reilly Webcast: Developing Applications for Palm webOS</a>
* <a href="http://arstechnica.com/gadgets/news/2009/02/vmware-mvp-windows-and-android-on-the-same-phone.ars">VMware MVP: Windows and Android&#8230; on the same phone [arstechnica]</a>

There were some chipsets announced, additionally, but I think that it is better to wait for the actual devices using them, as this isn't a blog aimed at hardware engineers.

BTW, Fennec/Firefox: I had a look at Fennec on my FreeRunner, and though the UI is nice, the most important experience was &#8220;slow&#8221;. This wasn't unlikely considering the FreeRunners ARMv4T core and the fact that it doesn't feature hardware accelerated graphics yet (Glamo and its bad NDA :( ), and I have to add, that I didn't use a swap partition/swap file why using it. So to really have an opinion on &#8220;Fennec on Freerunner&#8221; I need to have another, closer look at it&mdash;the upper are just some first impressions.

Additionally I should mention <a href="http://lists.openmoko.org/nabble.html#nabble-td2380769">FSO MS 5.1</a>, a bugfix release to MS 5, then <a href="http://freeyourphone.de/portal_v1/viewtopic.php?f=19&amp;t=929&amp;start=60#p10321">a statement by a source we can believe in that Openmokos GTA03 won't have NAND, but just a µSD instead</a>&mdash;but I think that that's it for last week, I am off to CeBIT on wednesday or thursday.

_Stay tuned, hope to see you again next saturday!_
