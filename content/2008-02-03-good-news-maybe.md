+++
title = "Good news.. maybe."
aliases = ["2008/02/good-news-maybe.html"]
date = "2008-02-03T21:14:00Z"
layout = "post"
[taxonomies]
tags = ["A910", "A910i", "EZX/LJ", "neuf"]
categories = ["hardware"]
[extra]
author = "Peter"
+++
Today NiZoX was so kind to point me to the fact that Neuf Cegetel has released a new phone: <a href="http://www.mobinaute.com/92972-voip-motorola-a910i-rejoint-gamme-twin-cegetel.html">The Motorola Twin A910i.</a>
<!-- more -->

It appears to use SIP and might even enable users to use built-in Wireless LAN to surf the internet, what is really great, as this doesn't work on the A910s that are out here, yet.

So I'm really looking forward to get my hands on the firmware of this device, I'm quite exited, as I believe that the hardware is the same.

If you've got an A910i want to help me, please <a href="http://www.e2mod.com/content/view/78/28/">get this tool</a>, install it on your A910i (copy it to SD, choose the file in built-in filemanager and it should install), run it, upload the files (maybe at <a href="http://rapidshare.com/">Rapidshare</a>) and leave me a comment. Thanks!

(BTW: This means I'll stop any work on A910's FW as long as I had no look at A910i's FW, but as I am not spending time on this ATM it is no real change.)
