+++
title = "Some thoughts on the mobile devices world as it is this year"
aliases = ["2009/07/08/some-thoughts-on-the-mobile-devices-world.html"]
author = "peter"
date = "2009-07-08T13:51:00Z"
layout = "post"
[taxonomies]
tags = ["Android", "ARM", "palm pre", "WebKit", "webos"]
categories = ["commentary"]
+++
We (as all those people interested in mobile computing with linux (or not)) have in some kinds a very interesting year&mdash;there are some promises of the past finally coming true: convergency is one example and linux appears to be about to take off finally to the mass markets with ANDROID and WebOS&mdash;for those really pocketable devices and as Google announced ChromeOS today, we might see more PCs (beginning from Smartbooks/Netbooks/Mini Notebooks) being delivered without the big enemy (MS Windows) on their harddisks.

So does this mean that the future will be bright and happy?

Well, it might be&mdash;as this means that OpenSource Software finally arrives at the industry and yes, some companys don't seem to use OpenSource technologies not just because they're cheap&mdash;instead it appears like they are seen as a chance for standardization&mdash;one example is the WebKit rendering engine, which is THE mobile browsing engine today, being used by many big players: Symbian, Google Android (meaning the companies that use these OS), Apple, Palm&#8230; So when there is something new included in WebKit, it is likely that it will be supported in the compiled WebKit browsers of these players soon after&mdash;which means, that web developers can use the techniques WebKit supports without worrying too much.

On the other hand the hardware market is pretty boring (just like the software market when just focussed on techniques and buzzwords), smartphones will become faster soon featuring the next generation of SoCs, ARM11 is about to say goodbye (ok, the nVidia Tegra will keep it alive in high end units for some time): ARM Cortex A8 (or even A9) is taking over, delivering more power for a richer mobile experience&mdash;a device like the HTC Hero is interesting on the software side (just say Flash, it's ugly but used a lot), but dissapointing as the hardware is no more that high end since the Palm Pre (WebOS might really take off, as soon the GSM/3G variant of the Pre is out) in on the market, and there are plenty other speedboats to come.

And the software market? Well, Android is ok now with Cupcake and will become grown up with Donut (2.0), lots of new APIs&mdash;we have to hope that it won't become too power hungry&mdash;And Palm's WebOS approach is great, as it is somehow closer to classical GNU/Linux&mdash;an all free WebOS clone with less eyecandy might be nice for the Openmoko community by the way&mdash;it finally really seems like web techniques are ready to be competition towards &#8220;real&#8221;/classical applications with HTML5 and its surrounding technologies.
