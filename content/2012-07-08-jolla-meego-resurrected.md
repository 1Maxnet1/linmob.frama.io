+++
title = "Jolla: MeeGo resurrected."
aliases = ["2012/07/08/jolla-meego-resurrected"]
author = "peter"
date = "2012-07-08T13:53:00Z"
layout = "post"
[taxonomies]
tags = ["html5", "Jolla", "Jolla Mobile", "MeeGo", "MeeGo Harmattan", "Mer Project", "nokia n9", "Qt", "Tizen"]
categories = ["software", "commentary"]
+++
_Only recently, being again (after using my Palm Pre Plus for one day again) tired of Android, I checked Nokia N9 prices again. Still too expensive for my liking, but there seemed to be no hope for the Nokia MeeGo device._

Yesterday, this changed. Jolla, a finish company being run by former Nokia employees, has announced to build some devices based on MeeGo. As you are likely to an attentive person, you will imidiately ask yourself: Now, what will they build on? MeeGo Harmattan, that was a MeeGo compliant Maemo? MeeGo, that is now Tizen, after having been merged with LiMo? Or MeeGo, that is now Mer? Well, it turns out that they will build on the last one, on Mer, and thus Qt.

Unfortunately, there isn't much more to tell right now. As Qt and MeeGo/Mer are certainly pretty mature already, I do really hope for some (hardware) announcements pretty soon. Because without Hardware, nothing much will emerge of JollaMobile&mdash;it will be just another OpenWebOS (yeah, it will be very different, but also a dead end).

I wish the Jolla guys all the best. As these are the guys that made the N9 happen, they can build a disruptive product. And that's really needed.

<p><strong>What you should read:</strong> 
<a href="http://bergie.iki.fi/blog/meego-diaspora/">MeeGo Diaspora</a> by Henri Bergius</p>

<p><strong>Source:</strong>

<a href="https://twitter.com/jollamobile">Jolla Mobile (twitter)</a>
<a href="http://www.linkedin.com/company/jolla">Jolla (LinkedIn)</a></p>

<p><strong>Join the discussion:</strong>

<a href="http://www.theverge.com/2012/7/7/3143099/jolla-meego-startup-ex-nokia-employees">The Verge</a>
<a href="http://talk.maemo.org/showthread.php?t=85315">talk.maemo.org</a></p>
