+++
title = "Weekly linmob round up (2): Not much Android on MWC"
aliases = ["2009/02/01/weekly-linmob-round-up-2-not-much-android-on-mwc.html"]
author = "peter"
date = "2009-02-21T08:10:00Z"
layout = "post"
[taxonomies]
tags = ["acer", "Android", "gnufiish", "htc", "LiMo", "MWC2k9", "symbian foundation", "weekly roundup", "windows_mobile"]
categories = ["weekly update"]
+++

Time for another quick roundup of what happened this week in the world of mobile Linux - it wasn't that much considering that this was the &#8220;Mobile World Congress&#8221; week

The Android-powered phones being announced are easy to name, as they are few:

* <a href="http://www.engadget.com/tag/HtcMagic/">HTC Magic</a> (likely to be T-Mobile G2 in US, will be sold by Vodafone in Europe)
* <a href="http://www.engadgetmobile.com/2009/02/18/general-mobiles-dstl1-android-phone-eyes-on/">General Mobile DSTL1</a> (glossy, WQVGA (resistive touch) Marvell PXA3xx powered dual-SIM)

...and then there was a chinese QVGA device running Android I saw on video this week, but I can't find it right now.

Additionally (ex E-TEN) <a href="http://www.engadgetmobile.com/2009/02/17/acer-working-on-two-android-phones-to-launch-this-year/">Acer</a> (who reintroduced the Glofiish/gnufiish DX900) and <a href="http://www.engadgetmobile.com/2009/02/17/samsung-at-least-three-android-phones-and-a-limo-handset-in-200/">Samsung</a> announced to be working on Android devices to be launched this year, while presenting solutions running other mobile OSes.

Aside from these Android running devices, there is still the LiMo foundation and platform&mdash;which I, to be honest, don't like a lot, as it just appears to be interesting for the industry, which shares knowledge in it  (like &#8220;how to take a way the power from the user&#8221; ;-) ), but not for the user who looks for a great, extensible platform&mdash;which <a href="http://www.businesswire.com/portal/site/home/permalink/?ndmViewId=news_view&amp;newsId=20090215005131&amp;newsLang=en">announced to grow even more</a>.

Astonishing: <a href="http://www.engadget.com/2009/02/16/gsm-palm-pre-spotted-with-vodafone-sim-card/">No GSM variant of the Palm Pre was announced</a>.

When you have a look at mobile Linux platforms, you have to have (at least when you are about to become a businessman one day, like I do ;-) ) a look at the competitors on the markets. On the market for smartphones, which are about to feature a rich internet experience as well, these are certainly the Symbian Foundation and Windows Mobile.

Besides adding new members, there were some <a href="http://www.engadgetmobile.com/tag/SymbianFoundation/">rather nice Symbian handsets</a> from Samsung (Omnia HD), Sony-Ericsson, LG and of course Nokia, who have made the Symbian Foundation possible&mdash;another hard competitor for Microsofts aging Windows Mobile, which is e.g. in my opinion really bad at multimedia and internet performance.

Of course Microsoft knows that their OS has its weaknesses, but as Windows Mobile 7 isn't ready yet, the &#8220;being forced to do something to avoid a huge loss of market share&#8221; <a href="http://www.microsoft.com/presspass/press/2009/feb09/02-16MWCPR.mspx">Microsoft guys announced Windows Mobile 6.5</a>, which is at least an improvement - I wasn't at MWC, but a friend of mine has a 6.5 rom on his good old HTC BlueAngel, so I'd say that I can talk about this. But I won't do that now, maybe later, as it isn't sensational anyway.

Besides this there has been a bunch of Microsoft powered new devices to be announced at MWC, of which I will mention the devices that actually were interesting, if they didn't run Windows Mobile.

HTC, a smartphone maker gaining more and more market share, has announced the <a href="http://www.htc.com/www/product/touchdiamond2/overview.html">Touch Diamond2</a> and its keyboarded, more business aimed brother, the <a href="http://www.htc.com/www/product/touchpro2/overview.html">Touch Pro2</a> (which is in fact in my opinion a grandchild of the HTC Universal). Most interesting change you will notice while comparing them to their predecessors:  They both got bigger screens - a 0.4&#8221; increase on the Diamond, and a 0.8&#8221; raise on the Pro (while adding 160x480 pixels in terms of resolution) - a fact I like, as bigger screens makes the devices more finger friendly.

The other manufacturer, who's devices weren't that hyped in press, which is quite new to the market, Acer, showed up some devices with bigger screens, too. Namely these are the <a href="http://www.engadgetmobile.com/2009/02/17/acer-f900-m900-x960-and-dx900-hands-on-with-video/">F900 and its keyboarded brother M900</a>, both featuring WVGA 3.8&#8221; screens, and the same SoC like the <a href="http://wiki.openmoko.org/wiki/GTA03">Openmoko GTA03</a> will most likely feature: <a href="http://semicon.samsung.de/applicationprocessors/spec.aspx">Samsung S3C6410</a>. So what about some more <a href="http://www.gnufiish.org/">gnufiish</a>?

_Hope to see you again next week&#8230;_
