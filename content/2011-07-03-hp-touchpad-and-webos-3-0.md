+++
title = "HP Touchpad - and webOS 3.0"
aliases = ["2011/07/03/hp-touchpad-and-webos-3-0.html"]
author = "peter"
date = "2011-07-03T18:07:00Z"
layout = "post"
[taxonomies]
categories = ["hardware", "commentary"]
tags = ["Android 3.0 (Honeycomb)", "Android tablets", "HP Touchpad", "HP webOS", "webos tablets"]
+++
<p>The HP TouchPad is available in the US and a whole lot of reviews are out. Of course I don´t have a review unit, which is sad but normal for a small blog - so I can´t share any first hand impressions, just comment on what others have shared on the TouchPad.

Hardware:

The TouchPad is thicker than the iPad2 or modern Android Honeycomb Tablets like the Samsung Galaxy Tab 10.1 - however, the main complaint is materials: The TouchPads back is made of glossy plastic, which, as you may know, feels rather cheap. Besides that, it isn´t the lightest tablet out there.

Software: 

Everybody likes webOS usability. webOS 3.0 on the Touchpad starts with around 300 apps, of which 50 are great apps - this is more than the Android Honeycomb tablets started with, which is good, too. There are some complaints though, too: The Skype integration is not perfect yet, so is the overall speed of the device, hickups are said to occur from time to time (despite the fast Qualcomm chip inside). HP stated, that these issues are supposed to be fixed in about a month with an OTA update - there is only one first impression though, and this is as its always been with (HP) webOS: Great ideas, not polished yet (I was going to write: &#8220;Great ideas poorly carried out&#8221; but that sounded to harsh without a deep dive into the TouchPad (e.g. a long hands on (a few hours or days)).

Still, comments and ratings on the HP TouchPad have been overly positive. It´s said to feel very &#8220;natural&#8221; in use, that some parts of its usability are like &#8220;that tune you can´t get out of your head&#8221;, e.g. the swipe up to close an application. In fact, i have read more than once that while not yet on the iPad 2´s level, the TouchPad is already a serious contender to the Honeycomb tablets, which are believed to have an overly complecated user experience. 
Whether this is enough to have TouchPad sales at the level where HP expects them, remains to be seen.

(Stay tuned for another article on HP webOS in general later today.)</p>
