+++
title = "LinBits 19: Weekly Linux Phone news / media roundup (week 46)"
aliases = ["2020/11/15/linbits19-weekly-linux-phone-news-week46.html", "linbits19"]
author = "Peter"
date = "2020-11-15T10:30:00Z"
layout = "post"
[taxonomies]
tags = ["Librem 5", "Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Manjaro", "Jumpdrive", "Plasma Mobile", "Purism", "Librem 5", "Unboxing", "Convergence", "Sxmo", "Linux Application Summit", "slem.os", "OpenSUSE", "postmarketOS", "Liri"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

New distribution releases, JumpDrive 0.6, a bigger battery for the Librem 5, Linux App Summit talks, Plasma Mobile's October Update and more.  _Commentary in italics._
<!-- more -->

### Software releases and improvements
* [Arch Linux ARM/DanctNIX Mobile has seen another release](https://github.com/dreemurrs-embedded/Pine64-Arch/releases). Most notable is that they clock the RAM lower now by default, so if you have had crashes previously while trying this distribution, you can now try it again.
*  [Manjaro have released Phosh beta 2](https://forum.manjaro.org/t/manjaro-arm-beta2-with-phosh-pinephone/36766), [downloads](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/). 
*  Also, if you have not been updating your [Mobian](https://www.mobian-project.org/) install in a while, because you [read about possible breakage](https://fosstodon.org/@mobian/105148135200374744): [You can apparently upgrade it now again(https://social.librem.one/@chrichri/105204574174115135). If you broke your install, current builds should be fine for starting over.


### Hardware announcements

### Worth reading
* FOSSphones: [Why Are Linux Phones So Important](https://fossphones.com/why-are-linux-phones-so-important/). _Nice read!_
* LinuxSmartphones: [JumpDrive OS flashing tool now supports PineTab and 3GB PinePhone](https://linuxsmartphones.com/pine64-flashing-tool-jumpdrive-adds-support-for-pinetab-and-3gb-pinephone/). _It kind of did before, but now there is a new release._
* Plasma Mobile: [Plasma Mobile update: October 2020](https://www.plasma-mobile.org/2020/11/12/plasma-mobile-update-october.html). _Impressive update. I am really looking forward to all this to stabilize._
  * Tux Phones: [⚡ Plasma Mobile gets major UI improvements, several bugfixes](https://tuxphones.com/plasma-mobile-linux-mobile-desktop-status-end-2020/). _Raffaele's summary of the above._
* Purism: [Librem 5 4500mAh Battery Upgrade](https://puri.sm/posts/librem-5-4500mah-battery-upgrade/). _This is great news. The phone can allegedly now last a work day in idle, not in suspend, which solves all these “Does it wake up fast enough when a call comes in” [problems that exist with suspend on the PinePhone](https://nitter.net/ManjaroLinux/status/1327614609826648066#m). Sure, in a way it’s a hack, but it’s a good one in my opinion._
  * LinuxSmartphones: [Purism upgrades the Librem 5 Linux phone battery to 4,500 mAh ahead of shipping](https://linuxsmartphones.com/purism-upgrades-the-librem-5-linux-phone-battery-to-4500-mah-ahead-of-shipping/). _Brad's take on the topic mentioned above._
* Gamey: [Pinephone: Liri App Collection. Browser, Text, Terminal, Files / ArchLinux ARM PhoshPinephone: Liri App Collection. Browser, Text, Terminal, Files / ArchLinux ARM Phosh](https://lbry.tv/@gamey:c/Pinephone-Liri-Apps:c). _Interesting post if you ever wondered about using Liri apps on your PinePhone._

### Worth watching

* Avisando: [PinePhone "Desktop mode" Phosh Manjaro](https://www.youtube.com/watch?v=of2I3cRY3VI). _Awesome video!_
* Avisando: [PinePhone - Pogo Pins](https://www.youtube.com/watch?v=geQf_CUlZzI). _If you ever wondered about the Pogo Pins, maybe this video helps._
* BitBooger: [How to install (flash) Mobian on (to) your PinePhone!](https://www.youtube.com/watch?v=0H-AIH0i1qY). _Great video, glad that someone takes on the "How to" business for everyday PinePhone chores. Unfortunately, there are few mistakes made, but I tried to point those out in the comments._
* GeoTech Land: [Ubuntu Touch OTA-14 & Pinephone Update](https://www.youtube.com/watch?v=nETF1jrxOP4). _Nice update!_
* Liliputing/Linux Smartphones: [PinePhone: KDE Neon with Plasma UI](https://www.youtube.com/watch?v=iBtYVE6k_-s). _It's rough._
* Jaquen hgar: [PinePhone Ubuntu Touch - Hands On](https://www.youtube.com/watch?v=ccnaPEokZUg). _This a german video detailing the current state of Ubuntu Touch on the PinePhone._
* Pako St: [OBS on Manjaro ARM from Pinebook Pro test capture of PinePhone](https://www.youtube.com/watch?v=iT6eL1_RBqk). _ARM Linux in action!_
* Prawnboy SG: [Running Arch Phosh on Pinephone](https://www.youtube.com/watch?v=32ja9LGd3X4). _I am glad that ArchLinuxARM comes with less apps preinstalled. Still, nice to see how well it performs if you really decide install everything you can think of._
* Privacy & Tech Tips: [Pinephone Signal Messenger Clone: Axolotl- What Works + Screenshots](https://www.youtube.com/watch?v=Wf64kPxs2Tk). _A follow up to last weeks video, sadly without video of Axolotl running._
* Tech Show MX: [Manjaro ARM Beta 2 con Phosh en PinePhone](https://www.youtube.com/watch?v=LuLV6JiJjGg). _If you speak spanish, you are going to enjoy this one._


#### Manjaro PinePhone Unboxings
* Elatronion: [Pinephone Manjaro Edition: The Perfect Phone for Nerds](https://lbry.tv/@Elatronion:a/pinephone-manjaro:2). _This is a really nice video, watch it, even if you usually don't care about unboxings._
* Bitbooger: [Unboxing the Pinephone Manjaro CE and checking out basic things](https://www.youtube.com/watch?v=k2zBBbjdxRY).
* MTS: [PinePhone64 smartphone - unboxing and first thoughts (aka Part 1)](https://www.youtube.com/watch?v=4yvn0sHH1co).
* Terminal_Heat_Sink: [Unboxing and First Look at PinePhone Manjaro Convergence Package Limited Edition Linux Smartphone](https://youtu.be/YbEI2hvzCKk).


#### Linux App Summit
* Umang Jain:  [Libcamera: Making complex cameras easy!](https://www.youtube.com/watch?v=Vm66MPVwec4), [Slides](https://conf.linuxappsummit.org/event/1/contributions/6/attachments/1/2/Libcamera-umang-jain.pdf). _If you care about reducing camera complexity on Linux systems, you are going to like this._
* Dalton Durst: [Brickless battery saving with Ubuntu Touch](https://www.youtube.com/watch?v=tt1U677h5kY), [blog post](https://daltondur.st/las2020/). _Great talk by Dalton that really __everyone__ who cares about Linux Phones should watch!_
* Also check their [YouTube channel](https://www.youtube.com/channel/UCjSsbz2TDxIxBEarbDzNQ4w) many more videos on App Stream, Flatpak and Flutter that may be interesting.

### Stuff I did


I made three videos:
* Manjaro Plasma Mobile on the PinePhone: Convergence (Dev build 201107): [PeerTube](https://devtube.dev-wiki.de/videos/watch/1d7249d4-e577-4c8c-9f64-74855792f019), [LBRY](https://lbry.tv/@linmob:3/manjaro-plasma-mobile-on-the-pinephone:e), [YouTube](https://www.youtube.com/watch?v=OgEnSd5w4cQ). I am going to make a similar video with Plasma Mobile running on postmarketOS Edge next week.
* Sxmo: Simple X Mobile on PinePhone: [PeerTube](https://devtube.dev-wiki.de/videos/watch/91c96859-0a65-401a-bc11-8aa0645ea676), [LBRY](https://lbry.tv/@linmob:3/sxmo-simple-x-mobile-on-pinephone:5), [YouTube](https://www.youtube.com/watch?v=c0-JatWCDuo). 
* slem.os: OpenSUSE on the PinePhone: [PeerTube](https://devtube.dev-wiki.de/videos/watch/ee924f40-b82e-435d-ac4a-70dd7e6bdfc2), [LBRY](https://lbry.tv/@linmob:3/slem-os-opensuse-on-the-pinephone:a), [YouTube](https://www.youtube.com/watch?v=G8mR52ulDvw). Bad timing here, I will definitely follow up once more works.

Unfortunately, that was basically all I managed to do.
