+++
title = "LinBits 66: Weekly Linux Phone news / media roundup (week 40/41)"
date = "2021-10-13T20:00:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "Librem 5", "Nemo Mobile", "DanctNIX", "postmarketOS", "Ubuntu Touch",]
categories = ["weekly update"]
[extra]
author = "Peter"
+++

_Summing up seven days in Mobile Linux:_  Nemo Mobile publish another Manjaro-based image for the PinePhone that shows a lot of progress, postmarketOS stable 21.06 gets a third Service Pack (plus: another great podcast episode and nice videos) and UBports needs you to step up! <!-- more --> _Commentary in italics._ 

### Software releases

* postmarketOS [21.06 Service Pack 3](https://postmarketos.org/blog/2021/10/10/v21.06.3-release/) is out, delivering some new software on top of that stable release! _My personal highlight is that new postmarketOS theme, which not only has a sexy OLED variant, but also one called paper for eInk, which is really cool._
* DanctNIX [have released](https://twitter.com/DanctNIX/status/1446702450338000900) new images of their [Arch Linux ARM based distribution](https://github.com/dreemurrs-embedded/Pine64-Arch/releases/tag/20211008), bringing a lot of software up-to-date!
* Nemo Mobile have [published another image](https://twitter.com/neochapay/status/1447857885904580611). _The progress is impressive. I've [played with this](https://twitter.com/linmobblog/status/1448030004261097480) for a bit, and GUI is getting really good. Sadly, I am unable to connect that 0.6 image to my networks, so I could not play to much with it. One day later, it suddenly worked (for both, Wifi and cellular, I even received an SMS), so do give it a try!_

### Worth noting
* PINE64 is [teasing something](https://fosstodon.org/@PINE64/107066759372529097) ahead of fridays October Community Update. _Might be a phone, or another non-SBC device._ <!-- I know more, but I can't tell you yet. I'll post something on friday. -->
* HomebrewXS [have announced](https://twitter.com/HomebrewXS/status/1448020256497283082) that they are going to release their SupernovaOS for PinePhone on the last minute of 2021.
* A brief [Librem 5 shipping update](https://forums.puri.sm/t/estimate-your-librem-5-shipping/11272/432). _I want to believe into the projected graph, but given component shortages and the current manufacturing crunch in China I have my doubts._
* If you want to donate to your favourite project, but you are broke all the time, make sure to vote for your favourite project at [linux30.b1-systems.de](https://linux30.b1-systems.de/). _postmarketOS and UBports are excellent options, but many of the other projects are great, too!_

### Worth reading
#### Software news
* This Week in GNOME: [#13 It begins...](https://thisweek.gnome.org/posts/2021/10/twig-13/).
* KDE Announcements: [KDE Ships Frameworks 5.87.0](https://kde.org/announcements/frameworks/5/5.87.0/).
* Nate Graham: [This week in KDE: 🎶 Continuous integraaaaaaation 🎶](https://pointieststick.com/2021/10/08/this-week-in-kde-%f0%9f%8e%b6-continuous-integraaaaaaation-%f0%9f%8e%b6/).
* Claudio Cambra: [Getting ready for KDE review — Kalendar devlog 18](https://claudiocambra.com/2021/10/10/getting-ready-for-kde-review-kalendar-devlog-18/). _An initial release is inching closer! Awesome!_
* Allan Day: [Platform Design Goings On](https://blogs.gnome.org/aday/2021/10/12/platform-design-goings-on/). _While this post is desktop centric, these designs are also going to land in the Phosh world._

#### How To Contribute
* Nate Graham: [25 ways you can contribute to KDE](https://pointieststick.com/2021/10/13/25-ways-you-can-contribute-to-kde/). _I bet many items of this list apply to any other project, if KDE should not be your jam!_

#### Software review
* Gamey: [PINEPHONE: Reddit clients for Linux Phones!](https://gamey.tech/posts/pinephone-reddit-clients-for-linux-phones/). _Great post!_

#### Tutorials
* Amos B. Batto: [Tutorial to get started using Github](https://amosbbatto.wordpress.com/2021/10/12/tutorial-to-get-started-using-github/). _Good guide!._
* Hackers Game: [Containerization on Linux Phones](https://www.hackers-game.com/2021/10/11/containerization-on-linux-phones/). _See the video below!_

### Worth listening
* postmarketOS Podcast: [#10 SWMO, Waydroid, XFCE4, MATE, msm-fw-loader, mkinitfs, OctoPrint](https://cast.postmarketos.org/episode/10-SWMO-Waydroid-XFCE4-MATE-msm-fw-loader-mkinitfs-OctoPrint/). _Another great episode, with Swmo being discussed too!_

### Worth watching
#### Gaming
* Raezroth Helmiern: [Pinephone Gaming Demo: Mupen64Plus (CLI) LOZ-OOT](https://www.youtube.com/watch?v=8yKKgmgmDe4).

#### Kalendar
* Niccolò Ve: [Kalendar: A New KDE ... Calendar App! And More](https://tube.kockatoo.org/w/aAXVyduUNBLY5meVhqeqwe).

#### Tutorials
* HackersGame: [Containerization on Linux Phones](https://www.youtube.com/watch?v=_86tEHCyMsY). _Nice: Phosh inside Phosh!_

#### Accessory advice
* ameriDroid: [Pinephone tempered glass screen protector installation video](https://www.youtube.com/watch?v=iyieC8GtAn0). _You could also get one of these glass protectors for the iPhone Xs Max/11 Pro Max._

#### Bad Rants
* Privacy X: [Pine64 Phone SUCKS! Another Garbage Linux Device](https://www.youtube.com/watch?v=CBOcCWimdFU). _I will never understand why people get a second one, if the first wasn't for them. On the other hand, while I understand that the politics of going with Plasma Mobile make it a good choice, it's just not the best or most reliable experience for new users (yet) - a decision that certainly contributed to this rant._

#### Containers
* HackersGame: [Containerization on Linux Phones](https://www.youtube.com/watch?v=_86tEHCyMsY)

#### Ubuntu Touch
* UBports: [Ubuntu Touch Q&A 110](https://www.youtube.com/watch?v=81lCGwyTCP0). _Dalton, Marius and Alfred meet again to discuss what's new with Ubuntu Touch. Make sure to listen to the whole thing, or at least make sure to not miss Dalton's discussion of the big question he mentions in the beginning starting at 32:00. Also, do contribute!_
* Disty: [Blender and GIMP Running on Ubuntu Touch - Redmi 4X](https://www.youtube.com/watch?v=4C4gtfAcXIU). _You can, but you don't have to ;-)_
*  Techder Audios: [Ubuntu Touch On Oneplus One 2021 "GREAT PRIVACY SOFTWARE!?"](Ubuntu Touch On Oneplus One 2021 "GREAT PRIVACY SOFTWARE!?").

#### postmarketOS
* caleb: [OnePlus 6 postmarketOS update October 2021](https://www.youtube.com/watch?v=N3tcablm9DE). _Great video!_
* Martijn Braam: [postmarketOS on the OnePlus 6](https://spacepub.space/w/vRQY2RqDUKRYJCE6j9kyxv). _Nice!_
* Baonks81: [postmarketOS Phosh Redmi 2 wt86047 kernel 5.14](https://www.youtube.com/watch?v=WHFsXZgX9z0). _This postmarketOS OLED theme is really popular these days :-)_


#### Shorts
* NOT A FBI Honey pot: [HOW to install fedora on the pinephone..... #pine64 #shorts #linux](https://www.youtube.com/watch?v=n04stzWi8Ak). 
* NOT A FBI Honey pot: [only SIGMA MALES use arch on the PINEPHONE #sigma #shorts #linux #pine64 #pinephone #opensource](https://www.youtube.com/watch?v=spjD1DxV0y0).

### Stuff I did

#### Content
I did not manage to finish the post detailing my current Linux Phone setup yet. I hope to do so soon, and stay tuned for a blog post on friday!

#### Random 
I played for a bit with my Motorola Harpia (G4 Play) again. Turns out: Updating lk2nd every once in a while is quite important! 

#### LINMOBapps
Another low activity week (I wasted some time unsuccessfully trying to build one app). 
[Read here what (else) happened](https://framagit.org/linmobapps/linmobapps.frama.io/-/commits/master) on LINMOBapps this week. And please [do contribute!](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/CONTRIBUTING.md)

#### Linux Phone Apps

No progress, sorry!
