+++
title = "Computex 2011 - A Round Up / The Highlights"
aliases = ["2011/06/05/computex-2011-round-up-hightlights.html"]
date = "2011-06-05T00:35:00Z"
[taxonomies]
categories = ["events", "commentary"]
tags = ["ARM", "ASUS Eee Pad MeMO", "ASUS PadFone", "ASUS UX21", "Canoe Lake", "computex 2011", "Intel", "Intel Atom", "Key Lake", "MeeGo", "pixelqi", "tradeshows", "ultrabooknews", "Ultrabooks", "Windows 8", "windows on arm", "ZTE Light 2"]
[extra]
author = "Peter"
+++
_This years Computex is almost over, but I still haven't posted anything about it. (I didn't finish my post on Google I/O as well, but that's another story.)_
<!-- more -->
I think I am able to sum up the whole Computex 2011 up in one post, though. There weren't many highlights that fit into a blog that is about Linux on mobile devices in my humble opinion. Android tablets have been available for months, dual core smartphones are in the electronic stores as well - and the next mayor iteration of Google Android, &#8220;Ice Cream Sandwhich&#8221; which will unite smartphones and tablets again, is still to far away to see any devices on a consumer electronics tradeshow (leaks may happen soon).

### Hardware:
#### Intel
The Intel vs. ARM (including SoC manufaturers like nVidia, Samsung, Texas Instruments, Qualcomm and many others) battle continues. Intel and partners showed off many new netbooks built around the newest Atom platform &#8220;Cedar Trail&#8221;, which will be slightly more performant and much less power hungry. The real interesting thing that Intel introduced is a new category of ultra mobile powerhouses built around Intels &#8220;Sandy Bridge&#8221; and  (later) &#8220;Ivy Bridge&#8221; platforms - devices only slightly larger than netbooks while being a lot thinner and powerful than your average netbook. Intel call these devices Ultrabooks and I've started a blog specifically on this new category of devices: <a href="http://ultrabooknews.net/">Ultrabooknews.net</a>

Intel was rather silent, and I didn't stumble on any news of new Intel powered smartphones. Apparently Intel has nothing to offer that could beat the new ARM dual core superphones, &#8220;Medfield&#8221; isn't ready yet, and &#8220;Moorestown&#8221; was spotted in some new tablets, just like &#8220;Oak Trail&#8221;.

Let's get back to netbooks: MeeGo will run on some extra cheap netbooks like the ASUS EeePC X101 - featuring a new &#8220;Pine Trail&#8221; version, the N435 running at 1,33&#160;GHz - and while I love the fact that MeeGo will come preinstalled on netbooks, I can't recommend these devices: They are underpowered and most likely aren't that much cheaper (than standard netbooks) that they're worth buying.

#### ARM
Quadcores are coming and Cortex A15 designs as well - all in 2011. Graphics are one key differentiator between all these upcoming SoCs - and I actually recommend those featuring ARM Mali Graphics, if they aren't too inferior in performance:  <a href="http://blogs.arm.com/multimedia/249-making-the-mali-gpu-device-driver-open-source/">ARM is more likely to offer open graphic drivers</a> than Imagination Technologies (PowerVR SGX family), nVidia or Qualcomm and have one other differentiating feature: <a href="http://en.wikipedia.org/wiki/OpenCL">OpenCL</a>. Let's get back to Computex, though.

### Software:
#### MeeGo
MeeGo 1.2 is getting ready to roll out on netbooks. Tablets (or even smartphones) on MeeGo 1.2 will be relatively rare things, though, as at least the OpenSource UIs simply need some more time to mature (not to speak of real MeeGo apps).

#### webOS and Android
On <i>webOS</i> and <i>Android</i> there were even less news at Computex, and it's not necessary to mention that there were plenty of new not exactly Android devices on display at the tradeshow.

#### Competition:
Microsofts next OS, Windows 8, will, as you should already know, support the ARM platform as well. Microsoft showed off some UI teasers, which while close to Windows Phone 7's popular &#8220;Metro UI&#8221; really looks very promising and sounds great technically (HTML5 instead of Silverlight), so promising that I am really exited about it and hope for Open Source platforms to adopt some of the features of it, e.g. the two apps aside thing. Of course &#8220;old style&#8221; apps will be supported as well on the - it's going to run on everything.
It's the first time ever that MSFT managed to get me really exited about their stuff.

### Devices
My two favorite devices of this years Computex were shown by ASUS: The &#8220;Ultrabook&#8221; UX21 and the PadFone (which is exactly what its name suggests: A phone with a Pad Dock, much like the Motorola Atrix, only better.

Now it's time for the most important part of this roundup:

### The Video linklist:

* <a href="http://youtu.be/BHrcz7zcm_8">Windows 8 for Tablets - detailed demonstration (Netbooknews.com)</a>
* <a href="http://www.youtube.com/user/minipcpro#p/u/16/31Du4kdX0Cc">Malata Cedar Trail netbooks (Canoe Lake and a convertible Key Lake) (Netbooknews.com)</a>
* <a href="http://www.youtube.com/user/minipcpro#p/u/24/5CDZZQJxRxw">Acer M500 MeeGo Tablet (Netbooknews.com)</a>
* <a href="http://www.youtube.com/user/minipcpro#p/u/43/put2VPcsZWo">ASUS UX21 Hands On by Netbooknews.com</a>
* ASUS PadFone <a href="http://www.youtube.com/user/minipcpro#p/u/47/8lGm40pxwQo">Hands On</a> + <a href="http://www.youtube.com/user/minipcpro#p/u/49/ko6Ghi6XNTM">Details</a> (Netbooknews.com)
* <a href="http://www.youtube.com/user/minipcpro#p/u/45/pkEXLkkvlAs">ASUS EeePad MeMO (3D) Hands On (now with Honeycomb) (Netbooknews.com)</a>
* <a href="http://www.youtube.com/watch?v=GiwRGsokzJo">ZTE Light  2Tablet with 7&#8221; PixelQi screen (ARMdevices.net)</a>
* <a href="http://www.youtube.com/watch?v=EbL3-1vB73c">Solar powered PixelQi Tablet (ARMdevices.net)</a>
