+++
title = "My current setup with ArchLinuxARM/DanctNIX mobile"
aliases = ["2021/01/09/my-setup-with-danctnix-archlinuxarm.html"]
author = "Peter"
date = "2021-01-09T00:30:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "Arch Linux ARM", "DanctNIX Mobile", "Phosh", "Mobian", "Plasma Mobile", "libhandy", "setup", "Software choice", "Firefox", "SSB"]
categories = ["howto", "article"]

+++

_I promised to write about it in last [LinBits](https://linmob.net/2021/01/03/linbits26-weekly-linux-phone-news-week53.html), so here you go._
<!-- more -->
Recently, I decided to re-do my months old eMMC install of ArchLinuxARM/DanctNIX mobile, which has been my main PinePhone OS for quite a while now. I wanted to switch to the F2FS file system, and loose some clutter that had accumulated in various attempts to try out software for [LINMOBapps](https://linmobapps.frama.io), where I had neglected to write a PKGBUILD, but had just build and installed the program from source, making uninstall a tedious process. 

### Choosing default apps

I tried to be considerate and only install the programs I really need and use.

In particular, I installed:
* __Angelfish__ (plasma-angelfish, AUR): The Plasma Mobile browser that I use a lot.
* __Backups__ (deja-dup): To restore my backup and to backup my current setup. An alternative to this would be "Pika Backup", which is based on borg instead of duplicity.
* __Calindori__ (calindori-git (AUR)): Plasma Mobile calendar. I am going to [set it up to sync](https://dimitris.cc/kde/2020/12/30/Online_Calendars.html), but I didn't not get around to that yet.
* __Cawbird__ (cawbird-git (AUR)): Twitter client. I switched to git when the 1.3.x release appeared.
* __Evolution__ (evolution): Mail client. This is not adjusted for the mobile form factor at all, but once a few elements are hidden, it works ok. I mainly use it, because I had serious bad luck with Geary, which sometimes would not fetch my mails.
* __Image Viewer__ (eog): This really should be preinstalled.
* __Jami__ (jami-gnome): [Jami](https://jami.net/) is an interesting service (you may know it under its previous name GNU Ring); unfortunately I don't know anyone who uses it. It can also be a SIP app, altough calls did not work out-of-the-box for me (and I did not have the time to tinker yet).
* __mpv__ (mpv): Always good to have this one around.
* __NewsFlash__ (newsflash-git (AUR)): I use this to follow the news, as I am an RSS addict since I was first told about RSS. It's a Rust app, you can download my build [here](https://linmob.uber.space/pkgs/newsflash-git-731.9f4b901-1-any.pkg.tar.xz) (pacman -U filename) to install.
* __Nextcloud__ (nextcloud-client): Works terribly (scale-to-fit com.nextcloud.desktopclient.nextcloud on helps with initial setup), but I had to start using this as Notes (gnome-notes) would take ages to sync my Notes folder because it does not seem to cache locally. The good thing is: It only has to be setup once.
* __Notorious__ (notorious-git(AUR)): Replaced Gnome Notes as my editor when I switched over to the above sync solution. It's fine, but I might try ThiefMD (AUR: thiefmd) as well soon.
* __Password Safe__ (gnome-passwordsafe): Client to use KeePass databases. While it was dreadfully slow with my database earlier, this improved lately, so that I now longer need to use KeePassXC on my phone.
* __Pidgin__ (pidgin): This is a piece of really old GTK2-software, but as its _libpurple_ is what Chatty is building upon, I decided to add it to my setup. For it to run, Chatty has to be killed, which is why I had to add a script and a launcher to easily do this. Pidgin is interesting as it supports many services such as Discord, Signal, Threema etc. via plugin. I'll report back on this once I've set up some of these services.
* __Podcasts__ (gnome-podcasts): I recommend importing an OPML file you exported from your previous Podcast client for this app, as you would have to hunt for feed URLs (which for some podcasts is unacceptably hard) otherwise.
* __Quickddit__ (quickddit (AUR)): Reddit client, originally developed for Ubuntu Touch. It's quite nice, and I just prefer it's wider feature set to Giara, which would be more native on Phosh.
* __SuperTuxKart__ (supertuxkart): Once [`~/.config/supertuxkart/config-0.10/`](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/config/supertuxkart/config.xml) and the [launcher](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/desktop-files/fixes/supertuxkart.desktop) are adjusted, the game works ok, at least with the less graphically challenging maps. I think it's just running on the CPU and not using the GPU, but I am not sure about that. Also, the proximity sensor interferes with it, which is annoying, but there seems to be a [simple way to fix this](https://forum.pine64.org/showthread.php?tid=11168&pid=77588#pid77588). 
* __Telegram desktop__ (telegram-desktop):  I am not a massive fan of this service, but there are activities in my life that basically require using this. While I also have Telegram set up in Chatty (and thus Pidgin, which is way more useful as an interface than Chatty currently is), I sometimes start this app to browse Telegram. With a [modified .desktop file](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/desktop-files/fixes/telegramdesktop.desktop) it works just fine.
* __To Do__ (gnome-to-do-mobile, Manjaro package): This is the mobile-friendly downstream of Gnome To Do which basically is a simple To Do manager that integrates nicely with Nextcloud. I had to steal Manjaro's package here, I would have prefered to just steal their PKGBUILD but was unable to find it on their gitlab.
* __Tootle__ (tootle-git (AUR)): Mobile friendly Mastodon client. It's not perfect, but good enough for some light Mastodon interactions.
* __Weather__ (gnome-weather-git (AUR)): The mobile patches that have been available in Gnome Weather for PureOS and Mobian in a while seem to be making it upstream. The current version has issues when you install 
* __YouPlay__ (not packaged, [install script](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/scripts/youplay-installer.sh)): A simple YouTube music downloader. I rarely use it, but it was fun to try to write a stupid local installer. 
Also, I installed:
* wlr-randr and yad to use [the script in this old blog post by Purism](https://puri.sm/posts/easy-librem-5-app-development-scale-the-screen/). The script is quite helpful, and I look forward to doing more with yad in the future. Also, tor and tor-socks are installed, but that will follow later.

I removed:
* __Geary__ (geary-mobile): I use Evolution, see above.

### But how do you use Matrix?

While I can sometimes shut up about how and why [Matrix](https://matrix.org/) is great, I am an avid user of the platform. With this new setup, I decided to just go with Firefox webapps. I was using chromium before, but even with ozone and wayland I had keyboard issues &mdash; the keyboard would not pop up automatically, and worse, the o and k key would not work on every keyboard layout, thus rendering me unable to type my blogs URL (unacceptable!). Therefore, I decided to use Firefox, which is available in the current release (as opposed to ESR on PureOS or Mobian) on ArchLinuxARM. 
In order to use Firefox for this, I opted to create a seperate profile.

Setting up new profiles is relatively easy. Just navigate to `about:profiles` and click __Create a New Profiles__ to add a new one. Notice that you can not only name your profile, but that you can also choose the location where you want to safe it. I deciced to save my additional profiles under "ssb.default" and tor.default" in `~/.mozilla/firefox/`, in order to make the creation of launchers easier than it is if you have Firefox assign random names to the profile folders.
The main benefit of this is, aside from having a seperate profile for "Site specific browsers", which is a feature that you need to enable in `about:config` (set `browser.ssb.enabled` to `true`, is that you can customize many of the other settings, e.g.
* the user-agent: general.useragent.override in `about:config`, maybe to avoid the Android user-agent that comes with `firefox-mobile-config` and leads to many unsoliceted "use our Android app" recommendations;
* customize the proxy, be it for VPN or Tor; 
* enable specific plugins that you might not want to use all the time, but for certain use cases, e.g. _NoScript_ (which is handy, but hard to configure on the mobilized desktop Firefox on the PinePhone.

For Matrix, I created two launchers: One for [Hydrogen](https://hydrogen.element.io), which is an official mobile front-end supporting a limited featureset ([desktop file](https://framagit.org/linmobapps/linmobapps.frama.io/-/raw/master/desktop-files/ssb/hydrogen.desktop)); the other for the proper, full featured [Element app](https://app.element.io), which does not work too well on the default dpi, but is fine once scaling is set to 1.75 or 1.5.[^1]
The main reason for this is compatibility and to have less different interfaces for Matrix. While Fractal and NeoChat are both great and native on Phosh and Plasma Mobile respectively, neither of them supports end-to-end encryption (E2EE) without hacks like [Pantalaimon](https://wiki.mobian-project.org/doku.php?id=fractal#how-to-install), which does not perform to well once you use many chats. [Mirage](https://github.com/mirukana/mirage) thus is currently the best option, and I like it a lot, but I ran into issues with accessing encrypted conversations that I hope to avoid by using first party clients and only these.

### Thoughts and Conclusions

This setup is not perfect, but it mostly works for now. I would really like to have Full Disk Encryption, a feature that's not easily available on Arch Linux ARM yet. I thought about switching to Mobian for that, but then I would have to miss out Plasma Mobile, Calindori and Quickddit &mdash; or at least have a harder time obtaining these apps. Also, if I were to travel, I could switch Gnome Maps for [Pure Maps](https://www.flathub.org/apps/details/io.github.rinigus.PureMaps) and add [KTrip](https://invent.kde.org/utilities/ktrip) and [Itinerary](https://invent.kde.org/pim/itinerary). [KDE Connect](https://invent.kde.org/network/kdeconnect-kde) could also be a handy addition (it works well), but my desktop getting battery warnings from the PinePhone at 85% charge let me to not install it again without needing it. For FOSDEM, I am going to re-install [Confy](https://confy.kirgroup.net/) (confy-git); if I should be running Plasma Mobile by then, I would instead opt for [Kongress](https://invent.kde.org/utilities/kongress).

Looking at what I [considered necessary early on](https://linmob.net/2020/06/27/41-minutes-of-PinePhone-content.html), I would say that we're pretty close:
* I could run Org-Mode in Emacs (although Emacs with it's modifier-heavy commands is difficult to use with a software keyboard), 
* were I less lazy, I would have already set up GPG email with Evolution,
* Threema, Signal can both be "solved" via Anbox, but I will try to explore the possibilities around `libpurple` for now,
* quassel-client should work fine at a 150% scaling once the chat list is in its own window.

I'd wish that Phosh would crash less often, and the kernel itself could also loose a few small bugs. All things considered though, I must say that I am quite happy with the current state of the PinePhone.

[^1]: For more information on scaling, see [this blog post](https://linmob.net/2021/02/13/pinephone-setup-scaling-in-phosh.html).
