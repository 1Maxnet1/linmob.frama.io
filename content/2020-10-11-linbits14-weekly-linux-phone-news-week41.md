+++
title = "LinBits 14: Weekly Linux Phone news / media roundup (week 41)"
aliases = ["2020/10/11/linbits14-weekly-linux-phone-news-week41.html", "linbits14"]
author = "Peter"
date = "2020-10-11T00:25:00Z"
layout = "post"
[taxonomies]
tags = ["Pine64", "PinePhone", "PineTab", "PineCom", "Librem 5", "Purism", "Anbox", "Camera", "LINMOBapps", "MGLapps", "Fedora", "SailfishOS", "Phoc", "Megapixels", "Unboxing"]
categories = ["weekly update"]
+++

_It's sunday. Now what happened since last sunday?_

Phoc and Plasma Mobile updates, Fedora Mobility, "Fund Your App" and more. _Commentary in italics._
<!-- more -->

### Software releases and improvements 

* [phoc 0.4.3](https://source.puri.sm/Librem5/phoc/-/releases/v0.4.3). _A new release of the compositor with lots of xwayland improvements, which is likely going to help with convergence (as apps like GIMP or Krita still require xwayland) and other software (e.g. Anbox)._
* [phosh 0.4.5](https://source.puri.sm/Librem5/phosh/-/releases/v0.4.5). _This release adds a torch/flashlight quick setting, improves support for external screens and many fixes._[^1]
* [megapixels 0.11.1](https://git.sr.ht/~martijnbraam/megapixels/refs/0.11.1) brings further improvements, see blog post and commentary below.

### Worth Reading

* BrixIT Blog: [PinePhone Camera pt4](https://blog.brixit.nl/pinephone-camera-pt4/) _The progress with PinePhone camera support in the past week has been amazing. What's still missing is camera support in other apps, e.g. [Authenticator](https://gitlab.gnome.org/World/Authenticator) or in the browser for video conferencing and other purposes._
* LinuxSmartphones: [Fedora Mobility wants to bring the Linux distro to smartphones](https://linuxsmartphones.com/fedora-mobility-wants-to-bring-the-linux-distro-to-smartphones/). _It's nice that this happens, let's hope it leads to more activity and a better mobile Fedora. Make sure to also read the [original Mailing List announcement](https://www.mail-archive.com/devel-announce@lists.fedoraproject.org/msg02379.html) and their [Wiki page](https://fedoraproject.org/wiki/Mobility), as it mentions the Librem 5 and to my surprise the [OnePlus 5(T)](https://wiki.postmarketos.org/wiki/OnePlus_5_(oneplus-cheeseburger)) as further target devices._
* Purism: [Fund Your App to Vote for the Future](https://puri.sm/posts/fund-your-app-to-vote-for-the-future/). _Purism have made a new website](https://puri.sm/fund-your-app/) to get a better idea of which apps are needed by their customers and to gain the funding to develop these. At first I mistook this for a project to distribute money to developers and wondered about the developer story, but given [recent](https://www.reddit.com/r/Purism/comments/j687kb/purism_hires_gnome_developer_alexander/) and [future hirings](https://puri.sm/job/pureos-gtk-application-development/) by Purism this looks more like a 'give us money, and we will develop them' approach than something like a 'Bountysource for mobile Linux'. BTW: I decided to vote and do so with a small donation. Let's see how this develops!_
* Plasma Mobile: [Plasma Mobile update: September 2020](https://www.plasma-mobile.org/2020/10/09/plasma-mobile-update-september.html). _Great read! The most notable change in my book is the switch to the maliit2 keyboard, which supports GTK 3 apps. This means that (as long as there are no show stopping issues in other components, e.g. KWin) Plasma Mobile can now be used with a much larger app selection than before, which is great, as also Plasma Mobiles own apps are being improved bit by bit._
* TuxPhones: [PINE64 to launch PineCom, a cheaper, modem-free PinePhone-like PDA](https://tuxphones.com/pine64-pinecom-launch-cheaper-linux-pda/). _One more product by PINE64. I am not really sure, what to make of this yet – but if you are looking for a truly private device, this might be pretty interesting. [Brad's article](https://linuxsmartphones.com/pine64s-pinecom-will-be-like-a-smaller-cheaper-pinephone-but-not-a-phone/) is worth a read, too, and you should not miss the [forum thread](https://forum.pine64.org/showthread.php?tid=11772)._

### Worth Listening
* The Linux Cast: [Why Linux on Mobile Might be Doomed](https://anchor.fm/thelinuxcast/episodes/Why-Linux-on-Mobile-Might-be-Doomed-ekg17s). _This episode is 9 days old (and thus technically to old for inclusion), but I listened to it in the past week and it made me so angry, that I had to include it. Many facts are wrong, Anbox seems to be an unknown, and, and, and... It's worth a listen, because he makes some valid points. My position is: Who cares about market share, as long as Linux on Mobile can be a solution that works for you individually?_

### Worth Watching

* Liliputing: [PinePhone: How to install apps in postmarketOS (using a terminal)](https://www.youtube.com/watch?v=FNOWJOuaesg). _I bet I demoed that too, months ago, but videos like this are truly helpful for new users._
* Avisando: [PinePhone "p-boot"](https://www.youtube.com/watch?v=WK1LfZf5FtI). _Unfortunately, this video does not show more of the 13 distributions than [Megi's original video](https://www.youtube.com/watch?v=NqyuKysK6ag)._
* Avisando: [PinePhone - Samsung battery [DIY]](https://www.youtube.com/watch?v=KxYwUNeglPk). _Don't try this at home. I mean it._
* Switched to Linux: [PineTab Unboxing](https://www.youtube.com/watch?v=9zisqhlUrXI). _In case you care about the PineTab, here is a video!_
* Privacy & Security Tips: [Installing SailfishOS On Sdcard On Pinephone + Call/SMS Demo + Cam Attempt](https://www.youtube.com/watch?v=f1t4njImIXI). _If you want to try Sailfish OS on your PinePhone or are just curious on its current state, this video is for you._
* UBports: [Ubuntu Touch Q&A 86](https://www.youtube.com/watch?v=H4ef2HWVmQs). _Installer news, Halium 9 support, and more._[^1]



### Stuff I did
* I wanted to make a video on SailfishOS, but lacked the time and energy (I mispoke even more often and lost my train of thought than usual) – also the video linked above had curbed my enthusiasm. 
* Instead, I invested quite some time in [LINMOBapps](https://linmobapps.frama.io): I searched hard for AppStreamIDs, a search I [managed to finish](https://fosstodon.org/@linmob/105012771765704577) earlier today. Also, the list keeps growing – for now. My next task (after making a merge request to get my changes into MGLapps) is to remove apps that don't work or can't be build – e.g. the source for `choqok-kirigami` is unavailable as the developer deleted their GitHub account – and to put all the games on a different list. If you have an idea on how to improve the list, know an app that is missing, please get in touch.
* Also, I wrote a blog post on [Reddit clients](https://linmob.net/2020/10/06/reddit-clients-for-mobile-linux.html) and updated/amended [two](https://linmob.net/2020/09/05/pinephone-building-plasma-mobile-apps-from-the-aur.html) [older](https://linmob.net/2020/08/15/anbox-on-the-pinephone.html) posts.

[^1]:This item was added after initial publication on October 11th, 2020.
