+++
title = "CeBIT 2011: Hands on with Hanvon A116 7\" Android tablet"
aliases = ["2011/03/cebit-2011-hands-on-with-hanvon-a116-7.html"]
date = "2011-03-06T11:50:00Z"
[taxonomies]
tags = ["7 tablet", "Android 2.2 (Froyo)", "Android 2.3 Gingerbread", "ARMv7", "Hanvon A116", "video"]
categories = ["hardware", "impressions",]
[extra]
author = "peter"
+++

The device I played with for the longest time at CeBIT 2011 without being bothered by booth people was the 7 inch tablet <a href="https://linmob.net/tags/hanvon-a116/">Hanvon A116</a>. Despite its specs sheet says that this device has some decent hardware inside (1GHz ARM Cortex A8 (Samsung Hummingbird according to ARMdevices.net) + 512MB DDR2 Ram) this thing ran Android 2.2 in a horribly slow way&mdash;this maybe due to Hanvons customizations or bad drivers.  
<!-- more -->

Here's my video: [Hanvon A116 Hands On](https://www.youtube.com/watch?v=ArqHlfseAb8)

I apologize for the quality of the video I shot&mdash;but at least I typed the spec sheet data into its description&mdash;so it might be helpful to some people. <a href="http://www.netbooknews.com/21052/hanvon-hpad-a116-7inch-android-2-2-tablet-hands-on/">netbooknews.com/tabletblog.de shot a better video</a>, and the <a href="http://armdevices.net/2011/03/02/hanvon-hpad-a116-7-capacitive-android-tablet/">video from ARMdevices.net</a> is clearly the best.

</a>Hardware specs are nice, but nothing special: 1GHz ARM Cortex A8 SoC (unnamed), 512MB DDR2 Ram, 2G SLC NAND, MicroSD  up to 32GB, 7" 1024x600 wide angle TFT, 2D capacative (optional:  Electromagnetic Resonance) touch screen, front camera: 1.3 MP, rear  camera: 5MP with auto focus, WiFi 802&#160;b/g, Bluetooth 2.1 + EDR, Optional  built in 3G, GPS: optional, Battery: 3.7V 3300mAh LiPolmer (12.2 Wh)  for 6 hour video play, 8 hour reading estimated battery life, I/O: micro  USB, mini HDMI, 3,5 earphone jack, dimension: 199 * 126 * 11.8mm  

{{ gallery() }}

Interestingly enough the datasheet has information on anything you can think of but the weight&mdash;the device felt pretty light, on par with other 7" tablets (350 to 450 grams). 

Hardware aside, Hanvon has been doing some work on the Software&mdash;on the unit I went hands on with, parts of this ran annoyingly slow&mdash;in fact, the homescreen replacement ruined the whole user experience for me&mdash;it seems to run much better in the video Andrzej shot for Netbooknews.com, so this might have been a faulty device (I had this thought at CeBIT, but when I came back later, all tablets were switched off and wouldn't power on). But they also added in their own office solution and their ebook store (they are THE player in China's e-reading market), so they are trying to add value, which is always a good thing (as long as you can remove the &#8220;added value" if you don't like it, but with Android you should be able to do so).

No real information on pricing (&#8220;30% less than Galaxy Tab" (street price or launch price?)) and availability, though it might come to Europe, as Hanvon has a German subsidiary.
