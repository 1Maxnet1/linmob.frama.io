+++
title = "About" 
path = "about" 
template = "single.html"
pagenate_by = 0
+++

### What is LINMOB.net?

LINMOB.net is a blog on LINux on MOBile devices, that has been around for more than 10 years - on and off. Now, with new exciting products like the Pine64 PinePhone and Purisms Librem 5 scheduled to be shipped later this year, it has come back to report on all things GNU/Linux on mobile devices such as smartphones, tablets and smaller notebooks.

This site is currently hosted on [Framagit](https://framagit.org/linmob/linmob.frama.io). 

### More Content

#### Shortform
You can follow updates and shortform related content that is not worth a blog post on [Twitter](https://twitter.com/linmobblog) `@linmobblog` or in the [Fediverse](https://fosstodon.org/@linmob) `@linmob@fosstodon.org`. 

#### Videos
If you like visual content, make sure to check out our [video content](https://linmob.net/videos/)[^1].

### How can I contribute?

#### Content

If you have an idea for an article or have actually written something, you are welcome to send it to me via [email](mailto:articles@linmob.net). We can't offer payment, but you'll be credited. We're going to get back to you as soon as we can.

#### I don't have time, but I have money

If you want to contribute money, please support the people that develop software for the PinePhone and other Platforms, e.g.

* [UBports, who build Ubuntu Touch](https://ubports.com/donate),
* [Ondřej Jirman (Megi)](https://xnux.eu/contribute.html#toc-donations), who works on the PinePhone kernel,
* [Mobian, a great Debian-based distribution](https://liberapay.com/mobian/donate),
* [postmarketOS, the real Linux distribution for phones](https://postmarketos.org/donate.html), that runs (well, boots) on more phones every day,
* [Plasma Mobile](https://www.plasma-mobile.org/findyourway/#!/rootgroup/outreach), who build a great GUI for phones,
* [Lune OS](https://pivotce.com/author/webosports/), who are keeping webOS alive, and
* individual app developers!

If we've missed a great project that has a donation link and should be added here in your opinion, please tell us about it!

#### App List: LinuxPhoneApps.org

If you want to help with LinuxPhoneApps, please check the [README](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/README.md) and the open [issues](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) on that project and [its subprojects](https://framagit.org/linuxphoneapps).

####  Otherwise...

Please lend your time to the projects you like, try to communicate in a friendly way and be helpful. There are a million ways to contribute to opensource, by reporting, triaging bugs, by improving documentation or translations. Thanks!

### Current Issues with Bing (and Bing-based search engines)

Since May 2021 this website has been delisted by Microsoft Bing (and importantly Bing-based search engines, e.g., ecosia, DuckDuckGo and more). Peter has appealed this twice without success (seemingly unrelated to that linmob.net was listed again for five days in July), and [the second time the process was so dissatisfying](https://fosstodon.org/@linmob/106987445312737202) that he's not eager to try again. 

So if you use a Bing-based search engine, keep in mind that you won't find linmob.net's content on it. You can search this site on the [archive page](https://linmob.net/tags/) if you have JavaScript enabled.


[^1]: If the audio volume is too low on my older videos on LBRY or YouTube, please check these videos out on PeerTube, where I carefully re-uploaded my videos after increasing the volume.

