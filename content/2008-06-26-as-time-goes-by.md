+++
title = "As time goes by"
aliases = ["2008/06/as-time-goes-by.html"]
date = "2008-06-26T06:13:00Z"
[taxonomies]
tags = ["A910", "Android", "angstrom", "titchy mobile", "debian lenny", "development", "E2831", "htc_universal", "linmob", "mda_pro", "platforms"]
categories = ["personal", "projects"]
[extra]
author = "Peter"
+++
I have to admit that I haven't made any progress on A910's firmware in this month. That makes me kind of sad, but as there was plenty of other (somehow more important) stuff to so, must be considered OK. And there are still some <a href="http://www.motorolafans.com/forums/showpost.php?p=147968&amp;postcount=10">problems</a>.
<!-- more -->
My HTC Universal is still on Debian/Titchy Linux sometimes, but there is an issue with it: It becomes slower and slower, as time goes on, and I can't really tell you why. Of course I use apt-get sometimes, but is that the reason for everything becoming a lot slower?

Would be sad, if it was like that.

Anyway, I'd prefer an OpenEmbedded/Ångstrøm based distribution for the HTC, as these are more optimzed size regarding architecture and size - but it'd be great if this could use Titchys daemon, which does a good job.

BTW: Android is said to be delayed. <a href="http://phandroid.com/2008/06/23/android-delayed-depends-on-who-you-ask/">Or not?</a>

Meanwhile, my E2831 lies in its box and I don't know whether I should sell it (if you are interested, just leave a comment) or not&#8230;

That's it for now.
