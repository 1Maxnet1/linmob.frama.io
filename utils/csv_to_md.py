#!/usr/bin/env python3

from common import *

import argparse
from dataclass_csv import DataclassReader
from datetime import datetime


def article_version() -> str:
    import feedparser
    import re

    feed = feedparser.parse('https://linmob.net/feed.xml')
    for feed_entry in feed.entries:
        version = re.match(r'Weekly.*\((\d\d)/(20\d\d)\).*', feed_entry.title)
        if version:
            if int(version.group(2)) == datetime.now().year:
                return str(int(version.group(1)) + 1).zfill(2)
            else:
                return '01'


def main():
    default_output = '{}-{}.md'.format(datetime.now().year, article_version())
    parser = argparse.ArgumentParser(description='Convert .csv to .md.')
    parser.add_argument('--input', help='input filename (default: entries.csv)', default='entries.csv')
    parser.add_argument('--output', help='output filename (default: {})'.format(default_output), default=default_output)

    args = parser.parse_args()

    entries_by_category = {}
    with open(args.input, 'r') as f:
        entries = DataclassReader(f, Entry)

        for entry in entries:
            if entry.category not in entries_by_category:
                entries_by_category[entry.category] = []
            entries_by_category[entry.category].append(entry)

    with open(args.output, 'w') as f:
        f.write(
'''+++
title = "Weekly GNU-like Mobile Linux Update ({}/{})"
date = "{}"
draft = true
[taxonomies]
tags = []
categories = ["weekly update"]
[extra]
author = "auto"
+++

Don't forget to check https://xnux.eu/log manually :)
'''.format(article_version(), datetime.now().year, datetime.now())
        )

        current_category = Category.NONE
        for category, entries in entries_by_category.items():
            if category != Category.MISC.value:
                if category != current_category:
                    f.write('\n# ' + category + '\n')
                    current_category = category
                for entry in entries:
                    f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')
        # misc at the end
        if Category.MISC.value in entries_by_category:
            misc_entries = entries_by_category[Category.MISC.value]
            f.write('\n### Misc\n')
            for entry in misc_entries:
                f.write('- ' + entry.source + ': [' + entry.title + '](' + entry.url + ')\n')


if __name__ == "__main__":
    main()
