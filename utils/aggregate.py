#!/usr/bin/env python3

from common import *

import argparse
from dataclass_csv import DataclassWriter
from datetime import datetime
import feedparser
from time import mktime

keywords = {
    Category.TRASH: ['amd', 'intel', 'nvidia', 'via', 'loongson', 'valve', 'web review','linux am dienstag', 'radeon', 'krita', 'kali', 'termux', 'web review', 'wine', 'directx', 'pinebook','pinecil','rockpro', 'rock64', 'quartz64', 'rockpro64','pinecil','restaurant','microsoft','unlock','pubg mobile','godnemo'],
    Category.HARDWARE: ['librem', 'mnt', 'poco', 'surface go', 'zerophone', 'risc-v', 'rockchip', 'allwinner'],
    Category.GNOME: ['gnome','gtk','clutter','mutter','libadwaita'],
    Category.PINEPHONE: ['pinephone'],
    Category.RELEASES: ['release'],
    Category.MAUI: ['maui'],       
    Category.NEMO_MOBILE: ['nemo'],
    Category.MAEMO_LESTE: ['maemo', 'hildon'],
    Category.CAPYLOON: ['capyloon','firefoxos'],
    Category.PHOSH: ['phoc', 'phosh'],
    Category.PLASMA: ['kde', 'plasma','qt'],
    Category.SAILFISH_OS: ['sailfish'],
    Category.UBUNTU_TOUCH: ['ubuntu', 'ubports'],
    Category.KERNEL: ['kernel', 'megi'],
    Category.STACK: ['pipewire', 'wayland', 'freedreno', 'hantro', 'cedrus', 'mesa', 'lima', 'panfrost'],
    Category.MATRIX: ['matrix','fluffychat','nheko','hydrogen','element'],
    Category.DISTRIBUTIONS: ['arch', 'fedora', 'glodroid', 'maemo', 'manjaro', 'mobian', 'postmarket', 'sailfish',
                             'opensuse', 'ubuntu', 'openmandriva','gentoo', 'nixos', 'debian', 'devuan'],
    Category.WORTHNOTING: [],
    Category.PODCASTS: [],
    Category.VIDEO: [],
    Category.MISC: []
}

sources = [
    # PinePhone
    Source('PinePhoneOfficial (Reddit)', SourceType.RSS, 'https://www.reddit.com/r/pinephoneofficial/new/.rss',
           Category.PINEPHONE),
    Source('PinePhone (Reddit)', SourceType.RSS, 'https://www.reddit.com/r/pinephone.rss', Category.PINEPHONE),
    # Software releases
    Source('biktorgj Modem Firmware', SourceType.RSS,
           'https://github.com/the-modem-distro/pinephone_modem_sdk/releases.atom', Category.RELEASES),
    Source('Tow-Boot', SourceType.RSS, 'https://github.com/Tow-Boot/Tow-Boot/releases.atom', Category.RELEASES),
    Source('Megapixels', SourceType.RSS, 'https://gitlab.com/postmarketOS/megapixels/-/tags?format=atom',
           Category.RELEASES),

    # Distributions
    Source('Droidian Blog', SourceType.RSS, 'https://droidian.org/blog/index.xml', Category.DISTRIBUTIONS),
    Source('Purism: list of Apps that fit and function well', SourceType.RSS,
           'https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361.rss',
           Category.DISTRIBUTIONS),
    Source('Lemmy - postmarketOS', SourceType.RSS, 'https://lemmy.ml/feeds/c/postmarketos.xml?sort=Active',
           Category.DISTRIBUTIONS),
    Source('Maemo Leste', SourceType.RSS, 'https://maemo-leste.github.io/feeds/all.atom.xml', Category.DISTRIBUTIONS),
    Source('Mobian Blog', SourceType.RSS, 'https://blog.mobian-project.org/index.xml', Category.DISTRIBUTIONS),
    Source('Mobian Wiki', SourceType.RSS, 'https://wiki.mobian-project.org/feed.php', Category.DISTRIBUTIONS),
    Source('Mobile NixOS news', SourceType.RSS, 'https://mobile.nixos.org/index.xml', Category.DISTRIBUTIONS),
    Source('Nitrux', SourceType.RSS, 'https://nxos.org/feed/', Category.DISTRIBUTIONS),
    Source('postmarketOS Blog', SourceType.RSS, 'https://postmarketos.org/blog/feed.atom', Category.DISTRIBUTIONS),
    Source('Breaking updates in pmOS edge', SourceType.RSS, 'https://postmarketos.org/edge/feed.atom',
           Category.DISTRIBUTIONS),
    Source('UBports News', SourceType.RSS, 'https://ubports.com/blog/ubports-news-1/feed', Category.DISTRIBUTIONS),
    Source('glodroid_manifest (GitHub)', SourceType.RSS, 'https://github.com/GloDroid/glodroid_manifest/releases.atom',
           Category.DISTRIBUTIONS),
    Source('Pine64-Arch', SourceType.RSS, 'https://github.com/dreemurrs-embedded/Pine64-Arch/releases.atom',
           Category.DISTRIBUTIONS),
    Source('Kupfer', SourceType.RSS,
           'https://gitlab.com/kupfer/kupfer.gitlab.io/-/commits/main?format=atom', Category.DISTRIBUTIONS),
    Source('Manjaro PinePhone Plasma Mobile', SourceType.RSS,
           'https://github.com/manjaro-pinephone/plasma-mobile/releases.atom', Category.DISTRIBUTIONS),
    Source('PinePhone Fedora Image Generation Script ', SourceType.RSS,
           'https://github.com/nikhiljha/pp-fedora-sdsetup/releases.atom', Category.DISTRIBUTIONS),
    Source('Manjaro PinePhone Phosh', SourceType.RSS, 'https://github.com/manjaro-pinephone/phosh/releases.atom',
           Category.DISTRIBUTIONS),
    Source('Manjaro Blog', SourceType.RSS, 'https://blog.manjaro.org/feed/',
           Category.DISTRIBUTIONS),
    Source('Yocto Project Blogs', SourceType.RSS, 'https://www.yoctoproject.org/blogs/feed/',
           Category.DISTRIBUTIONS),
    #### Worth noting
    Source('fosstodon', SourceType.RSS, 'https://fosstodon.org/@linmob.rss', Category.WORTHNOTING),
    #### Gnome Ecosystem
    Source('This Week in GNOME', SourceType.RSS, 'https://thisweek.gnome.org/index.xml', Category.GNOME),
    Source('GNOME Shell & Mutter', SourceType.RSS, 'https://blogs.gnome.org/shell-dev/feed/', Category.GNOME),
    Source('Planet GNOME', SourceType.RSS, 'https://planet.gnome.org/atom.xml', Category.GNOME),
    #### Plasma Ecosystem
    Source('Bhushan Shah', SourceType.RSS, 'https://blog.bshah.in/feed.xml', Category.PLASMA),
    Source('Planet KDE', SourceType.RSS, 'https://planet.kde.org/atom.xml', Category.PLASMA),
    #### Sailfish OS
    Source('Community News - Sailfish OS Forum', SourceType.RSS, 'https://forum.sailfishos.org/c/community-news/25.rss',
           Category.SAILFISH_OS),
    Source('Jolla Blog', SourceType.RSS, 'https://blog.jolla.com/feed/', Category.SAILFISH_OS),
    #### Nemo Mobile
    Source('Nemo Mobile UX team', SourceType.RSS, 'https://nemomobile.net/feed.xml', Category.NEMO_MOBILE),
    Source('neochapay on twitter', SourceType.RSS, 'https://nitter.net/neochapay/rss', Category.NEMO_MOBILE),
    Source('Jozef Mlich on Fosstodon', SourceType.RSS, 'https://fosstodon.org/\@jmlich.rss', Category.NEMO_MOBILE),
    #### Capyloon
    Source('Capyloon', SourceType.RSS, 'https://capyloon.org/releases.xml', Category.CAPYLOON),
    #### Matrix
    Source('Matrix.org', SourceType.RSS, 'https://matrix.org/blog/feed', Category.MATRIX),
    Source('nheko (Matrix)', SourceType.RSS, 'https://github.com/Nheko-Reborn/nheko/releases.atom', Category.MATRIX),
    ### Worth listening
    Source('postmarketOS Podcast', SourceType.RSS, 'https://cast.postmarketos.org/feed.rss', Category.PODCASTS,
           MimeType.AUDIO),
    Source('PineTalk Podcast', SourceType.RSS, 'https://www.pine64.org/feed/mp3/', Category.PODCASTS,
           MimeType.AUDIO),
    ### Worth Watching
    Source('Dylan Van Assche', SourceType.RSS, 'https://tube.tchncs.de/feeds/videos.atom?videoChannelId=3457',
           Category.VIDEO, MimeType.VIDEO),
    Source('Phalio', SourceType.RSS, 'https://tube.tchncs.de/feeds/videos.atom?videoChannelId=5823', Category.VIDEO,
           MimeType.VIDEO),
    Source('fabrixxm', SourceType.RSS, 'https://peertube.uno/feeds/videos.atom?videoChannelId=8689', Category.VIDEO,
           MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22pinephone%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22librem%205%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22ubuntu%20touch%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22ubports%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22SailfishOS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22Sailfish%20OS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22postmarketOS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22postmarket%20OS%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22Maemo%20Leste%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22nemo%20mobile%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22droidian%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    Source('YouTube', SourceType.RSS,
           'https://search.trom.tf/search?q=%22mobian%22%20site%3Ayoutube.com&categories=videos&time_range=week&language=en-US&format=rss',
           Category.VIDEO, MimeType.VIDEO),
    # misc
    Source('Drew DeVaults blog', SourceType.RSS, 'https://drewdevault.com/blog/index.xml'),
    Source('Gamey', SourceType.RSS, 'https://gamey.tech/index.xml'),
    Source('Marius Welt', SourceType.RSS, 'https://marius.bloggt-in-braunschweig.de/feed/'),
    Source('Anjan Momi Homepage', SourceType.RSS, 'https://momi.ca/feed.xml'),
    Source('avery.cafe', SourceType.RSS, 'https://avery.cafe/index.xml'),
    Source('Blogger Bust', SourceType.RSS, 'https://bloggerbust.ca/index.xml'),
    Source('Blog on Dalton Durst', SourceType.RSS, 'https://daltondur.st/index.xml'),
    Source('JFs Dev Blog', SourceType.RSS, 'https://codingfield.com/index.xml'),
    Source('Martijn Braam', SourceType.RSS, 'https://blog.brixit.nl/rss/'),
    Source('Caleb Connnolly', SourceType.RSS, 'https://connolly.tech/index.xml'),
    Source('doof.me.uk', SourceType.RSS, 'https://www.doof.me.uk/feed/'),
    Source('eighty-twenty news', SourceType.RSS, 'https://eighty-twenty.org/index.atom'),
    Source('FOSSingularity', SourceType.RSS, 'https://fossingularity.wordpress.com/feed/'),
    Source('GabMuss Dev Log', SourceType.RSS, 'https://gabmus.org/index.xml'),
    Source('Hamblingreens Blog', SourceType.RSS, 'https://hamblingreen.gitlab.io/rss.xml'),
    Source('James Westmans blog', SourceType.RSS, 'https://www.jwestman.net/feed.xml'),
    Source('Jason123santas blog', SourceType.RSS, 'https://jasonsanta.xyz/rss.xml'),
    Source('Lemmy - linuxphones', SourceType.RSS, 'https://lemmy.ml/feeds/c/linuxphones.xml?sort=Active'),
    Source('LINux on MOBile', SourceType.RSS, 'https://linmob.net/feed.xml'),
    Source('linux phone on Bacardis cave', SourceType.RSS, 'https://bacardi55.io/categories/linux-phone/index.xml'),
    Source('matrix.org', SourceType.RSS, 'https://matrix.org/blog/feed'),
    Source('MobileLinux', SourceType.RSS, 'https://www.reddit.com/r/mobilelinux/.rss'),
    Source('Neil Brown', SourceType.RSS, 'https://neilzone.co.uk/feed/rss'),
    Source('PINE64official (Reddit)', SourceType.RSS, 'https://www.reddit.com/r/pine64official/new/.rss'),
    Source('Purism (Reddit)', SourceType.RSS, 'https://www.reddit.com/r/purism/new/.rss'),
    Source('Phones (Librem 5),- Purism community', SourceType.RSS, 'https://forums.puri.sm/c/librem/phones/11.rss'),
    Source('Phoronix', SourceType.RSS, 'https://www.phoronix.com/rss.php'),
    Source('PINE64', SourceType.RSS, 'https://www.pine64.org/feed/'),
    Source('PineGuild', SourceType.RSS, 'https://pineguild.com/feed/'),
    Source('project-insanity.org', SourceType.RSS, 'https://blog.project-insanity.org/feed/'),
    Source('Purism', SourceType.RSS, 'https://puri.sm/feed/'),
    Source('amosbbatto', SourceType.RSS, 'https://amosbbatto.wordpress.com/feed/'),
    Source('fossphones:main (GitHub)', SourceType.RSS, 'https://github.com/rav3ndust/fossphones/commits/main.atom'),
    Source('Spaetzblog', SourceType.RSS, 'https://sspaeth.de/feed/'),
    Source('TuxPhones - Linux phones, tablets and portable devices', SourceType.RSS, 'https://tuxphones.com/rss/'),
    Source('xnux.eu', SourceType.RSS, 'https://xnux.eu/rss.xml')
]

entries = []


def parse_sources(begin, end):
    for source in sources:
        if source.type == SourceType.RSS:
            feed = feedparser.parse(source.url)
            for feed_entry in feed.entries:
                timestamp = begin
                try:
                    if 'published_parsed' in feed_entry and feed_entry.published_parsed:
                        timestamp = datetime.fromtimestamp(mktime(feed_entry.published_parsed))
                    elif 'updated_parsed' in feed_entry and feed_entry.updated_parsed:
                        timestamp = datetime.fromtimestamp(mktime(feed_entry.updated_parsed))
                except ValueError:
                    pass
                if timestamp < begin or timestamp > end:
                    continue
                # if no title set, fallback to link
                if not 'title' in feed_entry:
                    feed_entry.title = feed_entry.link
                entry = Entry(feed_entry.title, feed_entry.link, timestamp, source.name, source.category.value)
                # determine category based on keywords if none set
                if source.category == Category.NONE:
                    full_content = ''
                    if 'content' in feed_entry:
                        for part in feed_entry.content:
                            full_content = full_content + part.value
                    for category, words in keywords.items():
                        if any(w in feed_entry.title.lower() for w in words) or any(
                                w in full_content.lower() for w in words):
                            if entry.category != Category.NONE.value:
                                entry.category = Category.MISC.value
                                break
                            entry.category = category.value
                if entry.category == Category.NONE.value:
                    entry.category = Category.MISC.value
                entries.append(entry)


def main():
    global entries

    parser = argparse.ArgumentParser(description='Aggregate sources in one file.')
    parser.add_argument('--output', help='output filename (default: entries.csv)', default='entries.csv')
    parser.add_argument('--begin', type=lambda s: datetime.strptime(s, '%Y-%m-%d'), help='begin date (YYYY-MM-DD)')
    parser.add_argument('--end', type=lambda s: datetime.strptime(s, '%Y-%m-%d'), help='end date (YYYY-MM-DD)')

    args = parser.parse_args()

    parse_sources(args.begin, args.end)

    with open(args.output, 'w') as f:
        w = DataclassWriter(f, entries, Entry)
        w.write()


if __name__ == "__main__":
    main()
